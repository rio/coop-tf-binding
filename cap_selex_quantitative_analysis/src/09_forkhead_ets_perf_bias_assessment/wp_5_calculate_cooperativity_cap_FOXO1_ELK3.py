'''
Created on 12/10/2017, 2017

@author: Ignacio Ibarra Del Rio

Description:
'''

from lib.utils import *
from lib.plot_utils import *
from lib.RFacade import RFacade

df = DataFrameAnalyzer.read_tsv_gz(join("../../data/foxo1_ets1", 'FOXO1_ELK3_scores.tsv.gz'))
print df.head()

df['w'] = df['score.FOXO1_ELK3'] / ((df['score.FOXO1_AF'] + df['score.ETS_ETS1']) / 2.0)
df = df[(df['score.FOXO1_AF'] > 0.2) & (df['score.FOXO1_ELK3'] > 0.4)]
print df.sort_values('w', ascending=False)
print df.sort_values('score.FOXO1_ELK3', ascending=False)

df['x'] = df['score.FOXO1_AF']
df['y'] = df['score.ETS_ETS1']
df['size'] = df['score.FOXO1_ELK3']
df['color'] = df['w']# df['FOXO1_ELK3'] / ((df['FOXO1_AF'] + df['ETS_ETS1']) / 2.0)

# print df.shape
df = df.sort_values('w', ascending=False)

# print df[df['seqs.by.model'].str.contains('GTCAACAGGAAGT')]

print df.shape
# df = df[(df['cooperativity'] > 2.0) | (df['cooperativity'] < 1.0)]

min_color = 0.7 # min(df['color']) # 0.4
max_color = max(df['color'])
df = df.sort_values('color', ascending=True)
print df.head()
df['label'] = [r['seqs.by.model'] if r['seqs.by.model'] == 'AGTCAACAGGAAGTGC' else '' for ri, r in df.iterrows()]

print df.head()
df['shape'] = ''
df['fill'] = ''
print df['color']

# print df[(df['score.FOXO1_AF'] > 0.3) and (df['w'] > 1.0)]
RFacade.plot_scatter_ggrepel(df, '../../data/figures/foxo1_ets1/rel_aff_ggrepel_test.pdf',
                             xmin=0.2, xmax=.6, ymin=.5, ymax=1.0, w=4, h=5,
                             size_lab='FOXO1:ELK3',
                             size_breaks=[0.45, 0.5, .52], colorLow='white', colorHigh='red', colorMid='white',
                             colorMin=0.5, colorMax=1.1, title='FOXO1/ETS1',
                             size_breaks_labs=['0.45', '0.5', '0.52'],
                             color_lab='Cooperativity (w)',
                             breaks_color_range=[0.5, .75, 1.0], show_fill_guide=False,
                             xlab='FOXO1', ylab='ETS1', ggrepel_fontsize=2,
                             midpoint=0.75)