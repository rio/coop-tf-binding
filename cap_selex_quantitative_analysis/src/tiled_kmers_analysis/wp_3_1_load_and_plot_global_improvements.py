'''
Created on 5/31/2018

DESCRIPTION

@author: ignacio
'''

from lib.utils import *
from lib.plot_utils import *
from lib.RFacade import RFacade
from lib.SELEX.CAPSELEXAnalyzer import CAPSELEXAnalyzer
from lib.HumanTFs import HumanTFs
humantfs = HumanTFs.get_tf_names()
forkhead_tfs = set(humantfs[humantfs['DBD'] == 'Forkhead']['HGNC symbol'])
ets_tfs = set(humantfs[humantfs['DBD'] == 'Ets']['HGNC symbol'])

# bis-specific
for bispecific_motif_filter in [True, False]:
    if bispecific_motif_filter is True:
        continue

    symbols = set([s for s in CAPSELEXAnalyzer.get_composite_motif_enrichments()['symbol'] if len(s.split("_")) == 2])

    basedir = '/g/scb2/zaugg/rio/data/selex_analyses_output'
    paths = []


    print 'reading paths.'
    for d in listdir(basedir):
        if not d in symbols:
            continue
        print d
        if not isdir(join(basedir, d)):
            continue
        for d2 in listdir(join(basedir, d)):
            if not isdir(join(basedir, d, d2)):
                continue
            for f in listdir(join(basedir, d, d2)):
                if not 'kfold_results' in f or not '_pi' in f:
                    continue
                if bispecific_motif_filter:
                    if 'filter_bispecific_motifs' in f:
                        # if families are forkhead and ETS, then skip
                        tf1, tf2 = f.split("_")[:2]
                        if (tf1 in forkhead_tfs and tf2 in ets_tfs) or (tf1 in ets_tfs and tf2 in forkhead_tfs):
                            continue

                p = join(basedir, d, d2, f)
                # print exists(p), p
                paths.append(p)
    res = []
    for i, p in enumerate(paths):
        if i % 100 == 0:
            print i, 'out of', len(paths)
        if bispecific_motif_filter is False and 'filter_bispecific_motif' in p:
            continue
        delta = -3 if (bispecific_motif_filter and 'filter_bispecific_motif' in p) else 0
        df = DataFrameAnalyzer.read_tsv_gz(p)
        df['symbol'] = p.split("/")[-2]
        df['motif'] = p.split("_")[-7 + delta]
        df['relative.position'] = int(p.split("_")[-6 + delta].replace("pi", ''))
        df['kmer.length'] = int(p.split("_")[-5 + delta].replace("k", ''))
        res.append(df)
    res = pd.concat(res)
    res['k'] = res['symbol'] + "_" + res['motif']


    df = []
    for k, grp in res.groupby('k'):
        for model, grp2 in grp.groupby('label'):
            df.append([k, model, average(grp2['r2.test'])])
    df = pd.DataFrame(df, columns=['k', 'model', 'r2.test'])


    hm = df.pivot('k', 'model', 'r2.test')
    hm = hm[hm.min(axis=1) > 0]


    # save the table with the input of tested datasets
    supp = res[['symbol', 'motif', 'kmer.length', 'k', 'n']].drop_duplicates('k')

    supp.to_excel('../../data/supp_XX_dataset_description.xlsx')

    # datasets whose min performance was greater than zero
    non_neg_datasets = set(hm.index.to_series())
    DataFrameAnalyzer.to_pickle(non_neg_datasets, "../../data/non_negative_improvements_capselex_datasets.pkl")

    plot_i = 0
    for a, b in combinations(hm, r=2):
        break
        print a, b
        plt.subplot(3, 3, plot_i + 1)
        plt.scatter(hm[a], hm[b], s=1.0)
        plt.xlabel(a)
        plt.ylabel(b)
        plt.xlim([0, 1.0])
        plt.ylim([0, 1.0])
        plt.plot([0, 1.0], [0, 1.0], color='black')
        plot_i += 1
    plt.tight_layout()
    savepdf('/tmp/scatter')
    plt.close()

    tf_families = pd.read_csv("../../data/tf_families_JASPAR2014_9606_plus.tsv",
                              sep='\t', index_col=None)
    tf_fam = {k: v for k, v in zip(tf_families['name'], tf_families['family'])}

    comparisons = [[x1, x2] for x1, x2 in combinations(set(res['label']), 2)]

    all_pvals = []

    # here we want to store a table with all reported performances, for future reference

    # summarize as baplots
    ttest_ind_res = []
    medians_df = []
    for dataset, grp in res[res['k'].isin(non_neg_datasets)].groupby('k'):
        print dataset
        for model, grp2 in grp.groupby('label'):
            next_medians = []
            for k, grp3 in grp2.groupby('relative.position'):
                next_medians.append([k, np.median(grp2['r2.test'])])
            medians_df.append([dataset, model, k, median(next_medians)])

    medians_df = pd.DataFrame(medians_df, columns=['dataset', 'model', 'label', 'median'])

    hm = medians_df.pivot('dataset', 'model', 'median')

    # save table with performances
    hm.to_excel("../../data/supp_trim_and_summarize_r2_values_invitro.xlsx")

    hm['max'] = hm.max(axis=1)
    for c in hm:
        hm[c] = hm[c] / hm['max']

    melted= hm.melt()
    melted = melted[melted['model'] != 'max']
    cols_order =['1mer', '1mer+2mer', '1mer+shape', '1mer+2mer+3mer']
    # sns.swarmplot(data=melted,x='model', y='value', order=cols_order, color='gray', size=1, linewidth=0.1)
    ax = plt.subplot()
    sns.barplot(data=melted, x='model', y='value', order=cols_order, color='gray')
    plt.axhline(np.mean(melted[melted['model'] == '1mer+2mer']['value']), color='gray', lw=0.1, linestyle='--')
    plt.axhline(np.mean(melted[melted['model'] == '1mer+shape']['value']), color='red', lw=0.2, linestyle='--')
    plt.axhline(np.mean(melted[melted['model'] == '1mer+2mer+3mer']['value']), color='gray', lw=0.1, linestyle='--')
    plt.subplots_adjust(right=0.3, top=0.5)
    remove_top_n_right_ticks(ax)
    plt.xticks(rotation=45, ha='right')
    plt.ylim([0.5, 1.0])
    plt.ylabel('Fraction of 1mer+2mer+3mer R^2')
    savepdf("../../data/figures/fractional_performances")
    plt.close()


    for plot_i, next in enumerate(comparisons):
        xlab, ylab = next
        if len(xlab) > len(ylab):
            xlab, ylab = ylab, xlab

        if ylab != '1mer+shape':
            continue
        if xlab != '1mer':
            continue
        print ylab, xlab

        labs = {'1mer+shape', '1mer'}
        # labs = None
        if labs is not None:
            if xlab not in labs:
                continue
            if ylab not in labs:
                continue


        print ylab, 'vs', xlab
        medians_df = []

        diff_by_fam_pair = {}

        ttest_ind_res = []
        for dataset, grp in res[res['k'].isin(non_neg_datasets)].groupby('k'):
            # print dataset
            a = grp[grp['label'] == xlab]
            b = grp[grp['label'] == ylab]
            # print a.head()
            # print b.head()

            next_medians_x = []
            for k, grp2 in a.groupby('relative.position'):
                next_medians_x.append([k, np.median(grp2['r2.test'])])
            # print dataset, xlab, median(next_medians_x)

            next_medians_y = []
            for k, grp2 in b.groupby('relative.position'):
                next_medians_y.append([k, np.median(grp2['r2.test'])])
            # print dataset, ylab, median(next_medians_y)

            for k, grp2_a in a.groupby('relative.position'):
                medians_x = grp2_a['r2.test']
                medians_y = b[b['relative.position'] == k]['r2.test']
                if medians_y.shape[0] == 0:
                    continue
                # print dataset, len(medians_x), len(medians_x)
                ttest_ind_res.append([dataset, k] + list(ttest_ind(medians_y, medians_x)))

            medians_df.append([dataset, ylab, median(next_medians_y)])
            medians_df.append([dataset, xlab, median(next_medians_x)])

        medians_df = pd.DataFrame(medians_df, columns=['dataset', 'label', 'median'])

        ttest_ind_res = pd.DataFrame(ttest_ind_res, columns=['dataset', 'position', 't.stat', 'p.val'])
        ttest_ind_res['p.adj'] = RFacade.get_bh_pvalues(ttest_ind_res['p.val'])

        ttest_ind_res['seq.library'] = ["_".join(s) for s in ttest_ind_res['dataset'].str.split("_").str[:-1]]

        print "# composite motifs tested"
        print len(set(ttest_ind_res['dataset']))
        print '# total datasets'
        print len(set(ttest_ind_res['seq.library']))
        print '# significant improvements after adding DNA-shape'
        print len(set(ttest_ind_res[ttest_ind_res['p.adj'] < 0.1]['seq.library']))
        print '# unique TFs'
        print len(set([tf for dataset in ttest_ind_res['dataset'] for tf in dataset.split("_")[:2]]))
        print '# unique TF pairs'
        print len(set([((tf1 + "_" + tf2) if tf1 > tf2 else (tf2 + "_" + tf1)) for dataset in ttest_ind_res['dataset'] for tf1, tf2 in [dataset.split("_")[:2]]]))


        stop()
        merged = medians_df[medians_df['label'] == xlab].merge(medians_df[medians_df['label'] == ylab], on='dataset')
        print xlab, ylab
        merged['diff'] = merged['median_y'] - merged['median_x']
        print merged.sort_values('diff', ascending=False).head(20)
        print merged.sort_values('median_x', ascending=True).head(20)

        # improvements by family pairs
        # alone
        merged['tf1'] = merged['dataset'].str.split("_").str[0]
        merged['tf2'] = merged['dataset'].str.split("_").str[1]
        merged['tf1.family'] = merged['tf1'].map(tf_fam)
        merged['tf2.family'] = merged['tf2'].map(tf_fam)


        all_fams = set(merged['tf1.family']).union(merged['tf2.family'])
        diff_table = []
        for fam in all_fams:
            # print fam
            sel = merged[(merged['tf1.family'] == fam) | (merged['tf2.family'] == fam)]
            # if fam == 'Forkhead':
            #     sel = sel[(sel['tf1.family'] != 'Ets') & (sel['tf1.family'] != 'Ets')]
            # print sel.sort_values('diff', ascending=False).head(5)
            diff_table.append([fam, None, sel.shape[0], median(sel['diff'])])
            diff_by_fam_pair[fam] = sel

        # 2 combinations
        for fam1, fam2 in combinations(all_fams, 2):
            sel = merged[((merged['tf1.family'] == fam1) & (merged['tf2.family'] == fam2)) |
                         ((merged['tf1.family'] == fam2) & (merged['tf2.family'] == fam1))]
            diff_table.append([fam1, fam2, sel.shape[0], median(sel['diff'])])

            fam1, fam2 = [fam1, fam2] if fam1 > fam2 else [fam2, fam1]
            diff_by_fam_pair[fam1 + "+" + fam2] = sel

        # Family 1 WITHOUT FAMILY 2
        for fam1, fam2 in permutations(all_fams, 2):
            sel = merged[((merged['tf1.family'] == fam1) & (merged['tf2.family'] != fam2)) |
                         ((merged['tf1.family'] != fam2) & (merged['tf2.family'] == fam1))]
            diff_table.append([fam1, fam2, sel.shape[0], median(sel['diff'])])
            diff_by_fam_pair[fam1 + " (no " + fam2 + ")"] = sel

        # Family 1 and NO FAMILY 2
        for fam1, fam2 in permutations(all_fams, 2):
            sel = merged[((merged['tf1.family'] == fam1) & (merged['tf2.family'] != fam2)) &
                         ((merged['tf1.family'] != fam2) & (merged['tf2.family'] == fam1))]
            diff_table.append([fam1, fam2, sel.shape[0], median(sel['diff'])])
            diff_by_fam_pair["no " + fam1 + " (no " + fam2 + ")"] = sel

        # custom query (others -> no Homeodomain, no Forkhead, no Ets
        removed = {'Forkhead', 'Ets', 'Homeodomain'}
        sel = merged[~merged['tf1.family'].isin(removed) & ~merged['tf2.family'].isin(removed)]
        diff_table.append(['Others', None, sel.shape[0], median(sel['diff'])])
        diff_by_fam_pair['Others'] = sel

        diff_res = pd.DataFrame(diff_table, columns=['fam1', 'fam2', 'n', 'r2.median.diff'])
        # print diff_res[diff_res['n'] >= 4].sort_values('r2.median.diff', ascending=False).head(20)

        queries = ['Forkhead+Ets', 'Homeodomain', 'Forkhead (no Ets)',
                   'Ets (no Forkhead)', 'Others']

        colors_by_fam = ["#a6cee3", "#fb9a99", "#1f78b4", "#b2df8a", "#33a02c", "#e31a1c", "#fdbf6f", "#ff7f00",
                         "#cab2d6", "#6a3d9a"]
        colors_by_fam_dict = {family: color for family, color in zip(queries, colors_by_fam)}
        colors_by_fam_dict['Others'] = 'gray'

        plot_scatter = True
        plot_boxplot = True
        merged['family.group'] = np.where(((merged['tf1.family'] == 'Forkhead') & (merged['tf2.family'] == 'Ets')) |
                                          ((merged['tf1.family'] == 'Ets') & (merged['tf2.family'] == 'Forkhead')), 'Forkhead+Ets', '')
        merged['family.group'] = np.where(((merged['tf1.family'] == 'Forkhead') & (merged['tf2.family'] != 'Ets')) |
                                          ((merged['tf1.family'] != 'Ets') & (merged['tf2.family'] == 'Forkhead')), 'Forkhead (no Ets)', merged['family.group'])
        merged['family.group'] = np.where(((merged['tf1.family'] != 'Forkhead') & (merged['tf2.family'] == 'Ets')) |
                                          ((merged['tf1.family'] == 'Ets') & (merged['tf2.family'] != 'Forkhead')), 'Ets (no Forkhead)', merged['family.group'])
        merged['family.group'] = np.where(((merged['tf1.family'] == 'Homeodomain') | (merged['tf2.family'] == 'Homeodomain')),
                                          'Homeodomain', merged['family.group'])
        merged['family.group'] = np.where(merged['family.group'].str.len() > 0, merged['family.group'], 'Others')

        labels = [next[0] for next in sorted([[label, median(grp['diff'])] for label, grp in merged.groupby('family.group')], key=lambda x: x[-1])]
        df_tmp = []
        for lab in labels:
            df_tmp.append(merged[merged['family.group'] == lab].reset_index(drop=True))
        merged = pd.concat(df_tmp).reset_index(drop=True)


        if plot_scatter:
            fig = plt.figure(figsize=[3, 3])
            ax = plt.subplot()
            # plt.axes().set_aspect('equal')
            plt.grid(True)

            labels_legend = []
            for family_group in queries[::-1]:
                sel = merged[(merged['family.group'] == family_group)]
                color = colors_by_fam_dict[family_group]
                plt.scatter(sel['median_x'], sel['median_y'], color=color, edgecolors='black', s=8.0, linewidths=0.1)
                labels_legend.append(fam)
            # plt.scatter(merged['median_x'], merged['median_y'], s=6.0, c=merged['family.group'].map(colors_by_fam_dict), linewidths=0.1)
            handles, tmp = ax.get_legend_handles_labels()
            plt.legend(handles[::-1], queries, loc='lower right', fontsize=5,
                       handletextpad=0.01, frameon=False).set_zorder(0)
            plt.plot([0, 1], [0, 1], c='black')
            plt.xlabel(r'R' + "$^2$ " +  xlab, fontsize=8)
            plt.ylabel(r'R' + "$^2$ " +  ylab, fontsize=8)
            plt.xticks(fontsize=8)
            plt.yticks(fontsize=8)
            plt.xlim([0, 1])
            plt.ylim([0, 1])
            ax.set_axisbelow(True)
            ax.grid(color='gray', linewidth=0.5)
            plt.tight_layout()
            savepdf("../../data/figures/comparisons_scatter_only_scatter_%s_vs_%s" % (ylab, xlab))
            plt.close()
            # savepdf('/tmp/test')
            # plt.close()
            # plt.tight_layout()

        if plot_boxplot:
            ax = plt.subplot(1, 2, 1)
            queries = [q for q in queries if q in diff_by_fam_pair]

            for q in queries:
                print q, median(diff_by_fam_pair[q]['diff'])
            boxplot_pos = [diff_by_fam_pair[q]['diff'] for q in queries]


            medianprops = dict(linewidth=1.5, color='black')
            bp = plt.boxplot(boxplot_pos, patch_artist=True,
                             medianprops=medianprops, widths=0.75, showfliers=False)
            plt.setp(bp['boxes'], color='black')
            plt.setp(bp['whiskers'], color='black')
            plt.ylim([0.0, 0.35])
            plt.ylabel('$\Delta$' + "R" + "$^2$")
            plt.title(ylab + '.vs.' + xlab)
            plt.axhline(0.0, color='gray', linestyle='--')
            ax.spines['top'].set_visible(False)
            ax.spines['right'].set_visible(False)
            # ax.spines['bottom'].set_visible(False)
            # ax.spines['left'].set_visible(False)
            plt.xticks([i + 1 for i in range(len(queries))],
                       queries, rotation=45, ha='right',
                       fontsize=10)
            # highglight the families for which the p-val is lower than 0.05 (i.e. forkhead)
            counter = 0
            # colors_by_fam = ['#e41a1c', '#377eb8', '#4daf4a']

            for patch, fam in zip(bp['boxes'], queries):
                # print fam
                next_color = colors_by_fam[counter]
                print next_color
                if fam == 'Others':
                    next_color = 'Gray'
                patch.set_facecolor(next_color)

                for pi in range(len(queries), counter, -1):
                    ymax_positions = min(max([max(y) if len(y) > 0 else 0.0 for y in boxplot_pos], 0.2), .25 - 0.013 * (len(queries) - pi))
                    ymax_positions = [ymax_positions - 0.08 * (xi - 1) for xi in
                                      range(len(boxplot_pos))]

                    x1, x2, y1, y2= counter + 1, pi, \
                                    ymax_positions[counter] - 0.003, ymax_positions[counter]

                    pval = 1.0
                    try:
                        if len(boxplot_pos[x1 - 1]) > 0 and len(boxplot_pos[x2 - 1]) > 0:
                            pval = RFacade.get_wilcox_test_pval(boxplot_pos[x1 - 1], boxplot_pos[x2 - 1])
                    except Exception:
                        print 'problem calculating pvalues (probably nans...)'
                    all_pvals.append(pval)
                    print x1, x2, queries[x1 - 1], queries[x2 - 1], counter, y1, pval
                    if pval < 0.05:
                        print 'plotting'

                        plt.plot([x1, x1, x2, x2], [y1, y2, y2, y1], linewidth=1, color='k')
                        h = .0
                        padj = RFacade.get_bh_pvalues(all_pvals + [pval])[-1]
                        symbol = RFacade.get_pval_asterisks(RFacade.get_bh_pvalues(all_pvals + [pval]))[-1]
                        print x1, x2, queries[x1 - 1], queries[x2 - 1], counter, y1, padj

                        plt.text((x1 + x2) * .5, y2 + h, symbol, ha='center', va='bottom',
                                 fontsize=8)
                counter += 1

        sns.despine(top=True, right=True)
        # if plot_i >= 3:
        #     break
        # plt.tight_layout()
        savepdf("../../data/figures/comparisons_scatter_%s_vs_%s" % (ylab, xlab))
        plt.close()
        stop()
