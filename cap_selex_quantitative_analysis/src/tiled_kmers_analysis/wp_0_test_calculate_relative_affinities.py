'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *

from lib.SELEX.SELEXAnalyzer import SELEXAnalyzer

# These are dataframes indicating the enriched cap-selex motifs and datasets
cap = SELEXAnalyzer.get_datasets_description_capselex()
cm = SELEXAnalyzer.get_composite_motif_enrichments()

selected = cm[(cm['symbol'] == 'FOXO1_ETV4') & (cm['seed'] == 'RWMAACAGGAARNN')]
filename = list(selected['filename'])[0] # 'FOXO1_ETV4_3_AS_TCCGTA40NGCC.fastq.gz'


# cap selex fastq files
# /g/scb2/zaugg/rio/data/cap_selex_datasets_jolma_2015/fastq'
R0_selex_dir = '../../data/PRJEB20112' # this data has to be downloaded for CAP-SELEX data
fg_selex_dir = '../../data/fg_selex/capselex'

# This method generates the k-mer relative affinities table based on a foreground and background files
# (CAP-SELEX or HT-SELEX will select backgrounds from respective studies)
fg_kmers, IG = SELEXAnalyzer.calculate_kmer_enrichments(filename, k=9, dataset_type='CAP-SELEX',
                                                        R0_selex_dir=R0_selex_dir,
                                                        fg_selex_dir=fg_selex_dir)


# fg_kmers is the table with relative affinities for all k-mers calculated from fg and background files, respectively
print fg_kmers.head()