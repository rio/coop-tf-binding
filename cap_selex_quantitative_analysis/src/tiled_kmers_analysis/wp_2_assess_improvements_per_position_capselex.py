'''
Created on 5/10/2018

DESCRIPTION

@author: ignacio
'''

from sklearn.linear_model import Ridge

from lib.MLRGenerator import MLRGenerator
from lib.MachineLearning import MachineLearning
from lib.SELEX.SELEXAnalyzer import SELEXAnalyzer
from lib.SequenceMethods import SequenceMethods
from lib.ShapeAnalyzer import ShapeAnalyzer
from lib.plot_utils import *
from lib.utils import *

cm = SELEXAnalyzer.get_composite_motif_enrichments()


def calculate_r2_capselex(jobid=None, **kwargs):

    query = kwargs.get('query', None)
    cm = SELEXAnalyzer.get_composite_motif_enrichments()

    # in case of debugging put an explicit script.
    # subset_list = {'FOXO1_HOXB13', 'ETV5_FOXO1', 'FOXO1_HOXA13', 'GCM1_FOXO1', 'FOXO1_ELF1', 'FOXO1_ELK1', 'FOXO1_ETV4',
                   # 'FOXO1_ELK3'}

    supp = cm[['symbol', 'filename', 'seed']]
    supp = supp[supp['symbol'].str.contains('_') & (supp['filename'].str.contains('Not_Applicable') == False)].reset_index(drop=True)
    supp.to_excel("../../data/supp_XX_initial_datasets.xlsx")


    # subset_list = ['FOXO1_ELK3']


    # cm = cm[cm['symbol'].isin(subset_list)].reset_index(drop=True)

    cm['has.forkhead'] = cm['families'].str.contains('Fox') | cm['families'].str.contains('Forkhead')

    # to check bis-pecificity, only query Forkhead
    # cm = cm[cm['has.forkhead']].reset_index(drop=True)


    for ri, r in cm.iterrows():
        # if ri != 20:
        #     continue
        filename = str(r['filename'])

        nt_code = r['ligand sequence']
        pattern = r['seed']

        # print filename
        if query is None:
            jobid = ri
            if ri != jobid:
                continue
            # pass
        else:
            if not query in filename:
                continue

        print filename, nt_code, pattern

        cycle = int(filename.split("_")[2])
        filename_no_ext = filename.replace(".fastq.gz", '')

        tf1, tf2 = filename.split("_")[:2]
        code = tf1 + "_" + tf2

        # if not code in subset_list:
        #     continue
        output_dir = join('/g/scb2/zaugg/rio/data/selex_analyses_output', tf1 + "_" + tf2, filename_no_ext)
        print exists(output_dir), output_dir

        p = '/g/scb2/zaugg/rio/data/kmer_counts_selex_data/' + filename
        print exists(p), p


        # expensive step: calculate kmer counts between N and the length of the composite motif, save
        # infomration gains, etc.
        kmer_length = len(r['seed'])
        print tf1, tf2, kmer_length
        mm_order = 5
        k_fixed_length = 10

        info_gain_df = []
        fg_kmers_capselex_by_ki = {}
        for ki in range(5, min(kmer_length + 1, 14  )):

            if ki > k_fixed_length:
                continue



            print 'curr ki', ki, ', kmer length', kmer_length
            fg_kmers_capselex, next_IG = SELEXAnalyzer.calculate_kmer_enrichments(filename,
                                                                                  dataset_type='CAP-SELEX',
                                                                                  k=ki, mm_order=mm_order,
                                                                                  count_reverse=True)
            next_IG['kmer.length'] = ki
            info_gain_df.append(next_IG)

            # calculate IG in additional thresholds
            next_IG = SELEXAnalyzer.get_info_gain_from_kmers(fg_kmers_capselex, [25, 75, 125], log=False)
            next_IG['kmer.length'] = ki
            info_gain_df.append(next_IG)
            fg_kmers_capselex_by_ki[ki] = fg_kmers_capselex

        info_gain_df = pd.concat(info_gain_df)
        print info_gain_df
        hm = info_gain_df.pivot('counts.thr', 'kmer.length', 'IG')

        # given the IG values we plot a 3D scatter
        # plot
        plot_scatter_3d = False
        if plot_scatter_3d:
            # libraries
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(info_gain_df['counts.thr'], info_gain_df['kmer.length'], info_gain_df['IG'], c=info_gain_df['IG'],
                       s=60)
            ax.view_init(25, -5)
            plt.xlabel('counts threshold')
            plt.ylabel('kmer.length')
            ax.set_zlabel('IG')
            bkp_dir = '/g/scb2/zaugg/rio/data/selex_analyses_output'
            tf_output_dir = "_".join(filename.split("_")[:2])
            output_dir = join(bkp_dir, tf_output_dir, filename.replace(".fastq.gz", ''))
            savepdf(join(output_dir, 'scatter3d'))
            plt.close()

        # max_mismatches = 3

        selex = SELEXAnalyzer()
        max_mismatches = selex.get_allowed_mismatches_threshold(pattern) - 2
        # in addition, calculate all the single TF binding related files for these two cases
        check_mono_tfs = False
        fg_kmers_htselex_by_tf = {}
        if check_mono_tfs:
            paths_tf1, paths_tf2 = [SELEXAnalyzer.get_htselex_paths_by_tf(tf) for tf in [tf1, 'ETS1']]
            print paths_tf1
            print paths_tf2
            for paths_tf, tf_name in zip([paths_tf1, paths_tf2], [tf1, 'ETS1']):
                for htselex_path in paths_tf:
                    print htselex_path
                    htselex_filename = basename(htselex_path)
                    fg_kmers_htselex, nextIG = SELEXAnalyzer.calculate_kmer_enrichments(basename(htselex_filename), dataset_type='HT-SELEX',
                                                                                        k=test_k, count_reverse=True)
                    fg_kmers_htselex_by_tf[tf_name] = fg_kmers_htselex

            fg_kmers_capselex = fg_kmers_capselex_by_ki[test_k]

            merged = fg_kmers_capselex.sort_values('y', ascending=False).merge(fg_kmers_htselex_by_tf[tf1],
                                                                               on='seq')
            merged = merged.merge(fg_kmers_htselex_by_tf['ETS1'], on='seq')
            merged = merged[['seq', 'counts_x', 'affinity_x', 'affinity_y', 'affinity']]
            merged.columns = list(merged.columns[:-1]) + ['affinity_z']

            merged['diff'] = merged['affinity_x'] - merged['affinity_y'] # merged[['affinity_y', 'affinity_z']].max(axis=1))
            plt.subplot(2, 2, 1)
            plt.scatter(merged['counts_x'], merged['diff'])
            for s in ['CTGTAAACAGGAAGTGGAG',
                      'AAAACAACAGGAAGTATTT']:
                y = []
                all = []
                for si in range(len(s) - test_k + 1):
                    next = s[si: si + test_k]
                    print si, next
                    sel = merged[merged['seq'] == next]
                    if sel.shape[0] != 0:
                        all.append(sel)
                all = pd.concat(all).reset_index(drop=True)
                for labeli, label in enumerate(['affinity_x', 'affinity_y', 'diff']):
                    plt.subplot(2, 2, labeli + 2)
                    plt.plot(range(all.shape[0]), all[label])
                    plt.legend(['w5', 'w3'])
                    plt.title(label)
            savepng("../../data/figures/w5_and_w3")
            plt.close()

            seqs = ['CTGTAAACAGGAAGTGGAG',
                      'AAAACAACAGGAAGTATTT',
                      'AACAAAACAGGAAGTTAGA',
                      'TGACGCACCGGAAGTTAGG',
                       'TTGACGCAGGAAGTTAGG',
                      'TTTTAAAAGGGGCCCCTTT']
            legends = ['w5', 'w3', 'w2', 'w1', 'w1(C)', 'control']
            legends = legends[:2] + legends[3:4]
            for s in seqs[:2] + seqs[3:4]:
                foxo1 = fg_kmers_htselex_by_tf['FOXO1']
                ets1 = fg_kmers_htselex_by_tf['ETS1']
                capselex = fg_kmers_capselex
                all_affinities = []
                for ri, df in enumerate([foxo1, ets1, capselex]):
                    plt.subplot(4, 1, ri + 1)
                    y = []
                    calculate_average_single_variants = True
                    for si in range(len(s) - 9 + 1):
                        next = s[si: si + 9]
                        if calculate_average_single_variants:
                            added_options = set()
                            options = set([next[:i] + nt + next[i + 1:] for i in range(len(next)) for nt in 'ATCG'])
                            options = {opt for opt in options if opt == next}
                            affinities = df[df['seq'].isin(options)].sort_values('affinity', ascending=False)
                            affinities = affinities.head(3)
                            affinities = affinities['affinity']
                            print si, next, len(affinities), max(affinities), min(affinities), average(affinities)
                            y.append(average(affinities))
                        else:
                            diff_y = merged[merged['seq'] == next]
                            if diff_y.shape[0] == 0:
                                y.append(0)
                            else:
                                y.append(diff_y['diff'].values[0])
                                df.append(diff_y.reset_index(drop=True))
                    if not calculate_average_single_variants:
                        df = pd.concat(df).reset_index(drop=True)
                        print df
                    plt.plot(range(len(y)), y)
                    plt.ylim([0, 1])
                    all_affinities.append(y)
                plt.subplot(4, 1, 4)
                y = all_affinities[-1]
                y1, y2 = all_affinities[:2]
                y = [yi - max(a, b) for yi, a, b in zip(y, y1, y2)]
                plt.plot(range(len(y)), y)
                plt.ylim([0, 1])
            plt.legend(legends)
            savepng("../../data/figures/example_selex_fraction")
            plt.close()

            if train_ht_test_cap:
                fg_kmers_capselex['n.matches'] = fg_kmers_capselex['seq'].apply(SequenceMethods.get_matches, args=[next])
                fg_kmers_htselex['n.matches'] = fg_kmers_htselex['seq'].apply(SequenceMethods.get_matches, args=[next])
                sel_capselex = fg_kmers_capselex[fg_kmers_capselex['n.matches'] >= len(next) - max_mismatches]
                sel_htselex = fg_kmers_htselex[fg_kmers_htselex['n.matches'] >= len(next) - max_mismatches]

                clf_selex = MachineLearning.TrainRidgeRegressor(np.array(ShapeAnalyzer.get_X_1mer(sel_htselex['seq'])),
                                                                sel_htselex['affinity'])
                r2_cross_technology = MachineLearning.get_r2_score(np.array(ShapeAnalyzer.get_X_1mer(sel_capselex['seq'])),
                                                                   sel_capselex['affinity'], clf_selex)
                merged = sel_htselex.merge(sel_capselex, on='seq')
                print pearsonr(merged['affinity_x'], merged['affinity_y'])
                print htselex_filename1, r2_cross_technology

        ids_by_positions = {}

        # counts threshold
        # here, the counts threshold is reduced to 20
        counts_thr = 20

        # lowest k is defined as two positions from the reference value
        ref_pattern = r['seed']

        print 'here...'
        print len(ref_pattern) - k_fixed_length + 1

        for pi in range(len(ref_pattern) - k_fixed_length + 1):
            for filter_bispecificity_motifs in [False, True]:
                if filter_bispecificity_motifs is False:
                    continue

                output_path = join(output_dir, 'correlations_shape_affinity_' + filename_no_ext + "_" + ref_pattern +
                                   "_pi%i" % (pi) +
                                   "_k%i" % (k_fixed_length) + ".pdf")



                print output_path
                # if exists(output_path):
                #     continue
                mismatches_tolerance = 0

                output_path_perf = output_path.replace(".pdf", "_kfold_results_mismatches_%i" % (mismatches_tolerance) + ".tsv.gz").replace("correlations_shape_affinity_", '')
                if filter_bispecificity_motifs:
                    output_path_perf = output_path_perf.replace(".tsv.gz", '_filter_bispecific_motifs.tsv.gz')

                print output_path_perf
                print filter_bispecificity_motifs, exists(output_path_perf), output_path_perf
                if exists(output_path_perf):
                    continue


                pattern = ref_pattern[pi: pi + k_fixed_length]
                print pi, pattern

                lowest_k = max(len(pattern) - 1, 10)
                for si in range(len(pattern) - lowest_k + 1):
                    print si
                    next = pattern[si:si + lowest_k]
                    fg_kmers_capselex = fg_kmers_capselex_by_ki[lowest_k]
                    sel = SELEXAnalyzer.get_kmers_subset(fg_kmers_capselex, next, counts_thr=counts_thr)

                    print sel.head()
                    # filter bispecificity
                    if filter_bispecificity_motifs:
                        bispecific_motif = 'GACGC'
                        sel['bispecificity.pattern'] = None
                        for bispecific_motif_pos_i in range(len(bispecific_motif)):
                            bis_options = [bispecific_motif[:bispecific_motif_pos_i] + nt + bispecific_motif[bispecific_motif_pos_i + 1:]
                                           for nt in 'ACGT']
                            for s in bis_options:
                                sel['bispecificity.pattern'] = np.where(sel['seq'].str.contains(s), s, sel['bispecificity.pattern'])
                        print sel['bispecificity.pattern'].value_counts()
                        print 'before', sel.shape
                        sel = sel[pd.isnull(sel['bispecificity.pattern'])]
                        print 'after', sel.shape
                    ids_by_positions[si] = sel
                    print si, sel.shape

                # save selected Kmers
                all_selected_kmers = pd.concat(ids_by_positions.values())
                DataFrameAnalyzer.to_tsv_gz(all_selected_kmers,
                                            output_path.replace(".pdf",
                                                                "_selected_kmers_by_position_mismatches_%i" % (mismatches_tolerance) +
                                                                ".tsv.gz").replace("correlations_shape_affinity_", ''))

                # plot common kmers between positions
                for i, j in combinations(ids_by_positions.keys(), 2):
                    a, b = ids_by_positions[i], ids_by_positions[j]
                    print i, j, a.shape[0], b.shape[0], len(set(a['seq']).intersection(set(b['seq'])))

                output_dir_dna_shape = tempfile.mkdtemp()
                if not exists(output_dir_dna_shape):
                    mkdir(output_dir_dna_shape)
                print exists(output_dir_dna_shape), output_dir_dna_shape

                all = []
                corr_table = []
                coef_by_position = {}
                perf_changes_by_position = {}

                for i in sorted(ids_by_positions.keys()):
                    print 'next coefficient', i
                    df = ids_by_positions[i]
                    affinities = np.array(df['y'])
                    x1mer = ShapeAnalyzer.get_X_1mer(df['seq'])
                    x1mer_n_2mer = ShapeAnalyzer.get_X_1mer_2mer(df['seq'])
                    x3merE2 = ShapeAnalyzer.get_X_3merE2(df['seq'])
                    x1mer_n_2mer_3mer = ShapeAnalyzer.get_X_1mer_2mer_3mer(df['seq'])
                    xshapes = ShapeAnalyzer.get_shapes_from_DNAShapeR(list(df['seq']), overwrite=True, output_prefix=output_dir_dna_shape + "/shapes",
                                                                      infer_flanks=True, shapes=['1-MGW', '1-HelT', '1-ProT', '1-Roll',
                                                                                                 '2-MGW', '2-HelT', '2-ProT', '2-Roll'])
                    xshapes_no_flanks = ShapeAnalyzer.get_shapes_from_DNAShapeR(list(df['seq']), overwrite=True, output_prefix=output_dir_dna_shape + "/shapes",
                                                                                infer_flanks=False, shapes=['1-MGW', '1-HelT', '1-ProT', '1-Roll',
                                                                                                           '2-MGW', '2-HelT', '2-ProT', '2-Roll'])


                    print list(xshapes.columns)
                    print list(df['seq'])[0]

                    x1mer_n_shape = pd.concat([x1mer, xshapes], axis=1)
                    x1mer_n_shapenoflanks = pd.concat([x1mer, xshapes_no_flanks], axis=1)
                    x1mer_n_shape_n_3merE2 = pd.concat([x1mer, xshapes_no_flanks, x3merE2], axis=1)


                    x1mer_res = MachineLearning.KFoldRidgeRegression(np.array(x1mer), affinities,
                                                                     label='1mer', position=i)
                    x1mer_n_2mer_res = MachineLearning.KFoldRidgeRegression(np.array(x1mer_n_2mer), affinities,
                                                                            label='1mer+2mer', position=i)
                    x1mer_n_2mer_3mer_res = MachineLearning.KFoldRidgeRegression(np.array(x1mer_n_2mer_3mer), affinities,
                                                                                 label='1mer+2mer+3mer', position=i)
                    x1mer_n_shape_res = MachineLearning.KFoldRidgeRegression(np.array(x1mer_n_shape), affinities,
                                                                             label='1mer+shape', position=i)
                    x1mer_n_shapenoflanks_res = MachineLearning.KFoldRidgeRegression(np.array(x1mer_n_shapenoflanks), affinities,
                                                                                     label='1mer+shape.no.flanks', position=i)

                    x1mer_n_shapenoflanks_n_3merE2_res = MachineLearning.KFoldRidgeRegression(np.array(x1mer_n_shape_n_3merE2), affinities,
                                                                                             label='1mer+shape+3merE2', position=i)

                    print median(x1mer_res['r2.test'])
                    print median(x1mer_n_shape_res['r2.test'])
                    print median(x1mer_n_shapenoflanks_res['r2.test'])
                    print median(x1mer_n_2mer_3mer_res['r2.test'])
                    print median(x1mer_n_shapenoflanks_n_3merE2_res['r2.test'])


                    print x1mer_n_shape_res
                    print output_dir_dna_shape

                    perf_changes_by_position[i] = pd.concat([x1mer_res, x1mer_n_2mer_res, x1mer_n_2mer_3mer_res, x1mer_n_shape_res,
                                                             x1mer_n_shapenoflanks_res, x1mer_n_shapenoflanks_n_3merE2_res])
                    mlr = MLRGenerator()

                    next_deltas = mlr.get_rsquared_deltas(x1mer_n_shape, affinities, classifier=Ridge(random_state=500),
                                                          nfold=10)

                    # fit the model and get the coefficients
                    next_coef = ShapeAnalyzer.get_model_coefficients_1mer_n_shape(list(df['seq']), list(df['affinity']))
                    coef_by_position[i] = next_coef

                    next_deltas['position'] = i
                    all.append(next_deltas)

                    shape_feat_names = ['MGW', 'PROT', 'ROLL', 'HELT']
                    for plot_i, k in enumerate(shape_feat_names):
                        continue
                        table = []
                        next_feat = xshapes[[c for c in xshapes.columns if k in c]]
                        next_feat['y'] = affinities
                        next_feat = next_feat.sort_values('y', ascending=True)
                        for perc_i in range(0, 100, 10):
                            # print perc_i
                            range_start, range_end = np.percentile(next_feat['y'], perc_i), np.percentile(next_feat['y'], perc_i + 10)
                            sel = next_feat[(next_feat['y'] >= range_start) & (next_feat['y'] <= range_end)]
                            # print perc_i, range_start, range_end, sel.shape[0]
                            for c in sel:
                                table.append([c, k, perc_i + 10, np.mean(sel[c])])
                        for feat_i, c in enumerate(next_feat):
                            if not 'x' in c and not 'y' in c:
                                corr_table.append([i, k, c, feat_i, pearsonr(next_feat[c], next_feat['y'])[0],
                                                   pearsonr(next_feat[c], next_feat['y'])[0]])

                        quantiles = pd.DataFrame(table, columns=['feature', 'feature.id', 'decile', 'average.value'])
                        quantiles_hm = quantiles.pivot(index='decile', columns='feature', values='average.value')
                        quantiles_hm = quantiles_hm[[c for c in quantiles_hm if not 'x' in c]]
                        del quantiles_hm['y']
                        palette = sns.color_palette("Blues", n_colors=quantiles_hm.shape[0])
                        for e, next_color in zip(quantiles_hm.iterrows(), palette):
                            ri, r = e
                            y = list(r.values)
                            x = range(1, len(y) + 1)
                            plt.subplot(2, 2, plot_i + 1)
                            plt.plot(x, y, c=next_color)
                            plt.title(k)

                    # savefig(join(output_dir, 'values_by_quantile_' + str(i) + "_" + filename_no_ext))
                    plt.close()

                corr_table = pd.DataFrame(corr_table, columns=['position', 'feat', 'feat.id', 'feat.pos', 'spearman.rho', 'r.pearson'])


                plot_quantiles_by_feat = False
                if plot_quantiles_by_feat:
                    for ci, c in enumerate([c for c in x1mer_n_shape if 'MGW' in c]):
                        if 'x' in c:
                            break
                        print ci, c

                        next = pd.DataFrame()
                        next[c] = x1mer_n_shape[c]
                        next['affinity'] = affinities
                        next = next.sort_values('affinity', ascending=False)

                        concat = []
                        for i, df2 in enumerate(DataFrameAnalyzer.split(next, next.shape[0] / 10)):
                            df2['i'] = 10 - i
                            concat.append(df2)
                        df2 = pd.concat(concat)
                        print df2.head()
                        print df2.tail()

                        # auroc last groups vs others
                        auroc, auprc = MachineLearning.get_auroc_n_auprc(list([1 for yi in range(df2[df2['i'] == 10].shape[0])]) +
                                                                         list([0 for yi in range(df2[df2['i'] != 10].shape[0])]),
                                                                         list(df2[df2['i'] == 10][c]) + list(df2[df2['i'] != 10][c]))
                        print auroc, auprc

                        plt.subplot(3, 3, ci + 1)
                        plt.xlabel('')
                        plt.title(c + "\nR=%.3f" % (pearsonr(df2[c], df2['affinity'])[0]) + "\nAUROC=%.2f" % (auroc), fontsize=7)
                        sns.boxplot(data=df2, x='i', y=c, showfliers=False)


                    plt.tight_layout()
                    savepng('/tmp/test_MGW_vs_affinities.png')
                    plt.close()


                # save improvements per position
                x1mer_n_shape_res_all = pd.concat([next for next in perf_changes_by_position.values()])


                DataFrameAnalyzer.to_tsv_gz(x1mer_n_shape_res_all, output_path_perf)

                # if the DNA_shape directory exists, then remove it
                if exists(output_dir_dna_shape):
                    system('rm -rf ' + output_dir_dna_shape)

                continue
                # here we show the rest of results
                stop()



                all = pd.concat(all)

                for k in coef_by_position:
                    coef_by_position[k]['position'] = k
                coefs = pd.concat(coef_by_position.values())
                coefs['feature'] = coefs['feature.name'].str.split(".").str[0]
                # here plot the model coefficients
                n_feats = len(set(coefs['feature']))
                plot_i = 0

                cbar_ax = plt.subplot2grid([8, 3], [6, 0], rowspan=1, colspan=1)
                cbar_ax2 = plt.subplot2grid([8, 3], [7, 0], rowspan=1, colspan=1)

                for feat in shape_feat_names:
                    grp = coefs[coefs['feature'] == feat]
                    if not feat in {'MGW', 'PROT', 'HELT', 'ROLL'}:
                        continue
                    plt.subplot2grid((25, 3), (plot_i * 5, 0), rowspan=4, colspan=1)
                    grp['rel.position'] = grp['position'] + grp['feature.name'].str.split(".").str[1].astype(int)
                    grp['rel.position'] -= np.min(grp['rel.position'])
                    print grp
                    hm = grp.pivot('position', 'rel.position', 'coef')

                    order_frame = hm
                    sns.heatmap(order_frame, cmap='RdBu_r', cbar_kws={'label': 'L2-coef', 'orientation': 'horizontal'},
                                fmt='', annot_kws={'size': 4}, cbar_ax=cbar_ax)

                    vmax = max(abs(max(hm.max())), abs(min(hm.min())))
                    vmin = -vmax

                    plt.xticks([])
                    plt.yticks([])
                    plt.xlabel('')
                    plt.ylabel(feat)

                    plt.subplot2grid((25, 3), (plot_i * 5 + 4, 0), rowspan=1, colspan=1)
                    # plot the standard deviations of the estimates
                    if order_frame.shape[0] <= 1:
                        continue
                    std_devs = order_frame.std()
                    order_frame = hm
                    g = sns.heatmap(pd.DataFrame(np.max(std_devs) - std_devs).transpose(),
                                    cmap='Purples', cbar_kws={'label': 'rec(std)', 'orientation': 'horizontal'},
                                    fmt='', cbar_ax=cbar_ax2)
                    plt.xticks([])
                    plt.xlabel('')

                    plot_i += 1

                for plot_i, k in enumerate(shape_feat_names + ['ALL']):
                    print k
                    all['aligned.position'] = all['position'] + all['i']
                    # sum two to allow the ref to be the actual motif
                    sel = all[all['group'] == k]
                    sel['delta.add.shape'] = ((sel['r.sq.seq.and.shape.i'] - sel['r.sq.seq'])) * 100 # / sel['r.sq.seq'] * 100).abs()
                    sel['delta.remove.shape'] = ((sel['r.sq.shape.minus.shape.i'] - sel['r.sq.shape'])) * 100 # / sel['r.sq.shape'] * 100).abs()
                    sel['min.change'] = sel[['delta.add.shape', 'delta.remove.shape']].abs().min(axis=1)
                    sel['min.change'] = sel['delta.add.shape']


                    print sel.head()

                    hm = sel.pivot(index='position', columns='aligned.position', values='min.change')
                    hm_delta_r2 = sel.pivot(index='position', columns='aligned.position', values='min.change')
                    max_column = max(hm.columns)
                    hm = hm[sorted(hm.columns)]
                    print hm
                    ncols = hm.shape[1]

                    annot = [" " * pi + pattern[pi: pi + lowest_k] + " " * (len(pattern) - lowest_k - pi) for pi in range(len(pattern) - lowest_k + 1)]
                    annot_df = pd.DataFrame([[ci for ci in s] for s in annot])
                    print hm.shape
                    print annot_df.shape
                    plt.subplot2grid((5, 3), (plot_i, 1), rowspan=1, colspan=1)
                    sns.heatmap(hm, cmap='Greens', cbar_kws={'label': '$\Delta$' + "R" + "$^2$"},
                                vmin=0, vmax=5, annot=annot_df, fmt='',
                                annot_kws={'size': 4})

                    if plot_i == 0:
                        plt.title(filename + "\n" + ref_pattern)
                    else:
                        plt.title('')

                    DataFrameAnalyzer.to_tsv_gz(hm, join(output_dir, k + "_" + pattern + ".tsv.gz"))
                    plt.xticks([])
                    plt.yticks([])
                    plt.ylabel(k)
                    plt.xlabel('')

                corr_table['abs.pos'] = corr_table['feat.pos'] + corr_table['position']
                plot_i = 0
                for feat_id in shape_feat_names:
                    print plot_i, feat_id
                    grp = corr_table[corr_table['feat'] == feat_id]
                    plt.subplot2grid((5, 3), (plot_i, 2), rowspan=1, colspan=1)
                    hm = grp.pivot('position', 'abs.pos', 'r.pearson')
                    sns.heatmap(hm, cmap='RdBu_r', vmin=-0.5, vmax=0.5, cbar_kws={'label': 'R'})
                    plot_i += 1
                    plt.title('')
                    plt.xticks([])
                    plt.yticks([])
                    plt.ylabel(feat_id)
                    plt.xlabel('')
                plt.subplots_adjust(bottom=0.5)
                savepdf(output_path.replace(".pdf", ''))
                plt.close()

if __name__ == '__main__':
    BSubFacade.prepare_jobarray_scripts(__file__, calculate_r2_capselex, n_jobs=664, ngroup=1,
                                        n_jobs_max=664, mem=7500, qos='normal', queue='htc',
                                        time="24:00:00")

