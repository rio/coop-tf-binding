

from lib.utils import *
from lib.plot_utils import *
from lib.Correlator import Correlator
from scipy.interpolate import interp1d
from lib.ShapeAnalyzer import ShapeAnalyzer

best_models = DataFrameAnalyzer.read_tsv_gz(join("../../data/cap_selex_performances_summary",
                                                 "1mer_1mer+shape+flanks.tsv.gz"))
ylab = "1mer+shape+flanks"

models_dir = "../../data/models"
r2_delta_by_id = {}
from scipy.stats import fisher_exact
from lib.RFacade import RFacade

perf_delta_dir = join("../../data/aggregated_performances")



data_summary_bkp = "../../data/20171125_feat_averages_and_r2_aggregated_bkp.pkl"
# data_summary_bkp = '../../data/feat_averages_and_r2_aggregated_bkp.pkl'
if not exists(data_summary_bkp):
    for ri, r in best_models.iterrows():
        # print ri
        tf_code = join(r['kmer.code'])

        print tf_code

        file_basename = tf_code + ".tsv.gz"

        hm_path = join(perf_delta_dir, file_basename)
        if not exists(hm_path):
            continue

        r2_delta_df = DataFrameAnalyzer.read_tsv_gz(hm_path)

        print r2_delta_df
        r2_delta_df = r2_delta_df.dropna(1, how='all')
        next_means = r2_delta_df.mean()
        r2_delta_by_id[tf_code] = list(next_means)

    cPickle.dump(r2_delta_by_id, open(data_summary_bkp, "wb"))
else:
    r2_delta_by_id = cPickle.load(open(data_summary_bkp, 'rb'))

lengths_r2 = [len(r2_delta_by_id[k]) for k in r2_delta_by_id]

# the effect between max length and the actual spline
max_length = 1000 # 500
cubic_splines_feat = {}
cubic_splines_perf = {}
for k in r2_delta_by_id:
    y = r2_delta_by_id[k]
    r2 = r2_delta_by_id[k]
    # if len(y) < 8:
    #     continue
    x = range(len(y))

    f = interp1d(x, y, kind='cubic')
    xnew = np.linspace(0, len(x) - 1, num=max_length, endpoint=True)
    ynew = f(xnew)
    # normalize_values
    sum_ynew = sum(ynew)

    # do not normalize values
    # ynew = [yi / sum_ynew for yi in ynew]
    # invert if the maximum value is right from the first half

    max_position = sorted([[i, ynew[i]] for i in range(len(ynew))], key=lambda x: -x[1])
    if max_position[0][0] > len(ynew) / 2:
        ynew = ynew[::-1]
        r2 = r2[::-1]
    min_ynew, max_ynew = min(ynew), max(ynew)
    # substract min and divide by max
    # ynew = [(yi - min_ynew) / max_ynew for yi in ynew]
    ynew = [(yi - min_ynew) for yi in ynew]

    cubic_splines_feat[k] = ynew

    # do exactly the same for the r2 delta values
    x = range(len(r2))
    f = interp1d(x, r2, kind='cubic')
    xnew = np.linspace(0, len(x) - 1, num=max_length, endpoint=True)
    ynew = f(xnew)
    # normalize_values
    sum_ynew = sum(ynew)
    ynew = [yi / sum_ynew for yi in ynew]
    # invert if the maximum value is right from the first half
    # substract min and divide by max
    min_ynew, max_ynew = min(ynew), max(ynew)
    ynew = [(yi - min_ynew) / max_ynew for yi in ynew]
    cubic_splines_perf[k] = ynew

# save splines for clustering purposes


df = pd.DataFrame([cubic_splines_feat[k] for k in cubic_splines_feat],
                  index=[i for i in range(len(cubic_splines_feat.keys()))]) # "_".join(k.split("_")[:2]) for k in cubic_splines.keys()])
df_r2 = pd.DataFrame([cubic_splines_perf[k] for k in cubic_splines_perf],
                     index=[i for i in range(len(cubic_splines_feat.keys()))])

length_by_tf_bkp = "../../data/lengths.pkl"
if not exists(length_by_tf_bkp):
    motifs_dir = '../../data/pfm_jaspar'
    from lib.MotifAnalyzer import MotifAnalyzer
    motif_analyzer = MotifAnalyzer()
    motif_length_by_tf = {f.replace(".ppm", ""): len(motif_analyzer.get_ppm(join(motifs_dir, f))['A'])
                          for f in listdir(motifs_dir)}
    DataFrameAnalyzer.to_pickle(motif_length_by_tf, length_by_tf_bkp)
motif_length_by_tf = DataFrameAnalyzer.read_pickle(length_by_tf_bkp)
input_path = "../../data/20172511_cluster_averages_cubic_splined_aggregated_r2.tsv.gz"
pam_clusters_path = "../../data/20172511_cluster_averages_cubic_splined_nmedoids_aggregated_r2.tsv.gz"

DataFrameAnalyzer.to_tsv_gz(df_r2, input_path)

if True or not exists(pam_clusters_path):
    rfacade = RFacade()
    rfacade.pam_clustering(input_path, pam_clusters_path)
# here we have to run the clustering script 03_2_clustering_by_pam.R, and load the matrix
n_medoids = DataFrameAnalyzer.read_tsv_gz(pam_clusters_path)

# plot from 2 to 10
tf_families = pd.read_csv("../../data/tf_families_JASPAR2014_9606_plus.tsv",
                          sep='\t', index_col=None)
tf_fam = {k: v for k, v in zip(tf_families['name'], tf_families['family'])}

# plot a hm highlighting the amount of TFs per cluster
as_percentages = False
sns.set_style('white')
enrich_df_by_group = {}
for nk in range(2, 9):
    selected_fams = None
    sns.set_style('white')
    # we are going to plot only the # of clusters when k = 6
    cluster_ids = list(n_medoids['n.' + str(nk)])
    clusters_df = pd.DataFrame(cluster_ids, columns=['cluster.id'])

    # replacements = {1: 1, 2: 2, 5: 1, 3: 2, 4: 2}
    # nk = 2
    # clusters_df['cluster.id'] = [replacements[k] for k in clusters_df['cluster.id']]
    print df.head()
    print df.tail()
    df['median.r2'] = df.median(axis=1)

    df2 = pd.concat([clusters_df, df], axis=1)
    df2.index = [k for k in cubic_splines_feat.keys()]
    df2['tf1'] = [k.split("_")[0] for k in df2.index]
    df2['tf2'] = [k.split("_")[1] for k in df2.index]
    df2['fam1'] = [tf_fam[k.split("_")[0]] for k in df2.index]
    df2['fam2'] = [tf_fam[k.split("_")[1]] for k in df2.index]
    df2['motif'] = df2.index

    # important step: renumber cluster id from 1 to five according to the mean signal (R2)
    med_signal_cluster = {ci: median(grp['median.r2']) for ci, grp in df2.groupby('cluster.id')}
    df2['med.r2.cluster'] = [med_signal_cluster[ci] for ci in df2['cluster.id']]
    df2 = df2.sort_values('med.r2.cluster', ascending=False)

    df2['rank'] = df2['med.r2.cluster'].rank(method='dense', ascending=False)
    df2['cluster.id'] = df2['rank'].astype(int)
    del df['median.r2']

    print df2.head()

    DataFrameAnalyzer.to_tsv_gz(df2, join("../../data/clusters_summary", str(nk) + ".tsv.gz"))

    print df2.head()
    df2['length'] = [motif_length_by_tf[tf] for tf in df2['motif']]

    fig = plt.figure(figsize=(4, 3))
    sns.boxplot(x='cluster.id', y='length', data=df2, palette='Set2')
    plt.title("k-mer length distribution")
    plt.ylabel('k-mer length')
    plt.xlabel('medoids cluster')
    savefig("../../data/figures/medoids_k5_motif_length")
    plt.close()


    print df2.head()
    print df2.shape

    uniq_tf_pairs = {'_'.join(motif_code.split("_")[:2]) for motif_code in df2.index}
    print len(uniq_tf_pairs)
    uniq_tfs = {tf for tf_pair in uniq_tf_pairs for tf in tf_pair.split("_")}
    print '# of uniq TF pairs', len(uniq_tf_pairs)
    print "# of uniq TFs", len(uniq_tfs)
    print 'here...'
    arch_df = DataFrameAnalyzer.read_tsv("../../data/average_dna_turns_by_motif.tsv")

    motifs_lengths = []
    dna_turns = []
    for v in df2.index:
        k = "_".join(v.split("_")[:2])
        sel = arch_df[arch_df['motif.id'].str.contains(k)]
        if sel.shape[0] == 0:
            motifs_lengths.append(np.nan)
            dna_turns.append(np.nan)
        else:
            motifs_lengths.append(average(sel['motif.length']))
            dna_turns.append(average(sel['dna.turns.average']))

    df3 = pd.DataFrame([[l, t] for l, t in zip(motifs_lengths, dna_turns)],
                       columns=['motif.length', 'dna.turns'])
    df3['cluster.id'] = list(df2['cluster.id'])
    sel = df3[[not np.isnan(k1) and not np.isnan(k2) for k1, k2 in zip(df3['motif.length'],
                                                                       df3['dna.turns'])]]
    for i in range(nk):
        next = sel[sel['cluster.id'] == i + 1]
        plt.subplot(nk, 2, i * 2 + 1)
        # plt.hist(next['motif.length'])
        sns.boxplot(next['motif.length'], orient='horizontal')
        plt.title(average(next['motif.length']))
        plt.xlim([0, 30])
        if i + 1 != nk:
            plt.xticks([])
            plt.xlabel('')
        plt.subplot(nk, 2, i * 2 + 2)
        # plt.hist(next['dna.turns'])
        sns.boxplot(next['dna.turns'], orient='horizontal')
        if i + 1 != nk:
            plt.xticks([])
            plt.xlabel('')
        plt.title(average(next['dna.turns']))
        plt.xlim([0, 2.0])

    # print ranksums(sel[sel['cluster.id'] == 1]['motif.length'],
    #                sel[sel['cluster.id'] == 2]['motif.length'])
    # print ranksums(sel[sel['cluster.id'] == 1]['dna.turns'],
    #                sel[sel['cluster.id'] == 2]['dna.turns'])
    plt.close()

    # plot lengths per cluster
    df2 = df2[['cluster.id', 'tf1', 'tf2', 'fam1', 'fam2']]
    fig = plt.figure()

    for rowi, label in enumerate(['fam', 'tf']):

        # if label != 'tf':
        #     continue

        by_group = []
        for cluster_i, grp in df2.groupby('cluster.id'):

            print grp.head()
            # print cluster_i
            # print grp.head()
            if label == 'tf':
                for tf in tf_fam.keys():
                    by_group.append([cluster_i, tf,
                                       sum([tf1 == tf or tf2 == tf
                                            for tf1, tf2 in zip(grp[label + '1'], grp[label + '2'])])])

                    if tf == 'FOXO1':
                        print by_group[-1]
            else:
                for fam in set(list(df2['fam1']) + list(df2['fam2'])):
                    by_group.append([cluster_i, fam,
                                     sum([fam1 == fam or fam2 == fam
                                          for fam1, fam2 in zip(grp[label + '1'], grp[label + '2'])])])

                # print by_tf_name[-1]
        by_group = pd.DataFrame(by_group, columns=['cluster', label, 'n'])

        # print by_group[by_group['tf'] == 'FOXO1']
        by_group= by_group.pivot(index='cluster', columns=label, values='n')

        print by_group

        by_group.columns = [c.decode("utf8") for c in by_group.columns]
        # print by_group
        for tf in by_group.columns:
            if sum(by_group[tf]) == 0:
                del by_group[tf]
        by_group_counts = pd.DataFrame([r for ri, r in by_group.iterrows()],
                                         index=by_group.index, columns=by_group.columns)

        if as_percentages:
            for tf in by_group.columns:
                by_group[tf] = by_group[tf] / float(sum(by_group[tf])) * 100
        by_group = by_group.transpose()
        by_group['best.k'] = [sorted([[ki, vi] for ki, vi in zip(r.index, r.values)],
                                       key=lambda k: -k[-1])[0][0] for ri, r in by_group.iterrows()]

        # print by_group
        ordered_by_cluster_keys = list()
        selected_by_cluster_set = set()
        for nki in range(1, nk + 1):
            sel_tfs = by_group[by_group['best.k'] == nki].sort_values(nki, ascending=False).index
            for k in sel_tfs:
                ordered_by_cluster_keys.append(k)
                selected_by_cluster_set.add(k)
            # print nki, len(sel_tfs), sel_tfs
        # print ordered_by_cluster_keys
        del by_group['best.k']
        by_group = by_group.transpose()[[k for k in ordered_by_cluster_keys]]

        sum_all = sum(by_group.sum(axis=1))

        # print sum_all

        # enrichment tests by category:
        enrichment_tests_table = []
        # print by_group.head()
        n_tuple_enrich = 1 # this number decides if we want one or two-tuple groups for assessing enrichment
        if n_tuple_enrich == 1:
            for k in by_group.columns:
                values = by_group[k]

                # print values

                # for i in range(nk):
                #     print i, sum(by_group.loc[i + 1])
                for i, vi in enumerate(values):
                    # print by_group
                    fam_cluster = vi
                    tfs_cluster = sum(by_group.loc[i + 1])
                    fam_all_clusters, tfs_all_clusters = sum(values), sum_all

                    has_fam = df2[(df2[label + '1'] == k) | (df2[label + '2'] == k)]
                    no_fam = df2[(df2[label + '1'] != k) & (df2[label + '2'] != k)]

                    # print k
                    # print has_fam.head()

                    # select for unique cases
                    select_uniq = False
                    if select_uniq:
                        print 'Selecting uniq cases...'
                        # print 'before', has_fam.shape, no_fam.shape
                        uniq_cases = set()
                        select = []
                        for ri, r in has_fam.iterrows():
                            code = "_".join(ri.split("_")[:2])
                            if code in uniq_cases:
                                select.append(False)
                                continue
                            else:
                                uniq_cases.add(code)
                                select.append(True)

                        if len(select) != 0:
                            has_fam = has_fam[select]

                        # select for unique cases
                        uniq_cases = set()
                        select = []
                        for ri, r in no_fam.iterrows():
                            code = "_".join(ri.split("_")[:2])
                            if code in uniq_cases:
                                select.append(False)
                                continue
                            else:
                                uniq_cases.add(code)
                                select.append(True)
                        if len(select) != 0:
                            no_fam = no_fam[select]

                        # print 'after', has_fam.shape, no_fam.shape

                    a = has_fam[has_fam['cluster.id'] == i + 1].shape[0]
                    b = no_fam[no_fam['cluster.id'] == i + 1].shape[0]
                    c = has_fam.shape[0] - a
                    d = no_fam.shape[0] - b
                    sum_k = sum([a, b, c, d])

                    t = [k, i, a, b, c, d, sum_k] + list(fisher_exact([[a + 1, b + 1], [c + 1, d + 1]],
                                                                      alternative="greater"))
                    t = t + ([tf_fam[k]] if label == 'tf' else [k])
                    enrichment_tests_table.append(t)
            enrich_df = pd.DataFrame(enrichment_tests_table, columns=[label, 'i',
                                                                      'a', 'b', 'c', 'd', 'sum',
                                                                      'odds', 'p.val', 'tf.family'])
            enrich_df['p.val.adj'] = RFacade.get_bh_pvalues_python(enrich_df['p.val'])
            print enrich_df.sort_values('p.val', ascending=True)

            # print enrich_df[enrich_df['fam'] == 'Homeodomain']

        else:
            for e in combinations(by_group.columns, 2):
                for k in range(nk):
                    sel = df2[((df2['fam1'] == e[0]) & (df2['fam2'] == e[1])) |
                              ((df2['fam1'] == e[1]) & (df2['fam2'] == e[0]))]
                    fam_cluster = sel[sel['cluster.id'] == k + 1].shape[0]
                    fam_all_clusters = sel.shape[0]
                    tfs_cluster = df2[df2['cluster.id'] == k + 1].shape[0]
                    tfs_all_clusters = df2.shape[0]
                    # print fam_cluster, tfs_cluster, fam_all_clusters, tfs_all_clusters
                    t = [":".join(e), k] + [fam_cluster, tfs_cluster, fam_all_clusters, tfs_all_clusters] +\
                        list(fisher_exact([[fam_cluster, tfs_cluster],
                                                    [fam_all_clusters, tfs_all_clusters]],
                                                   alternative="greater"))
                    enrichment_tests_table.append(t)
            enrich_df = pd.DataFrame(enrichment_tests_table,
                                     columns=['code', 'nk', 'a', 'b', 'c', 'd', 'odds', 'p.val'])
            enrich_df['p.val.adj'] = RFacade.get_bh_pvalues_python(enrich_df['p.val'])
            print enrich_df.sort_values('p.val', ascending=True).head(20)


        by_group_counts = by_group_counts[[k for k in ordered_by_cluster_keys]]
        for c in by_group_counts.columns:
            by_group_counts[c] = [str(k) if k != 0 else "" for k in by_group_counts[c]]
        # sort heatmap by columns shifting always to provide the best cluster from 1 to 6


        enrich_df_by_group[label] = enrich_df

        save_enrich_df = True
        if save_enrich_df:
            next = enrich_df.copy()
            next = next.rename(index=str, columns={"i": "cluster.id",
                                                 'a': 'n.datasets.with.TF.fam.in.cluster',
                                                 'b': 'n.datasets.no.TF.fam.in.cluster',
                                                 'c': 'n.datasets.with.TF.fam.outside.cluster',
                                                 'd': 'n.datasets.no.TF.fam.outside.cluster',
                                                 'odds': 'odds.ratio'})

            next['cluster.id'] = next ['cluster.id'] + 1
            next = next.sort_values('p.val.adj', ascending=True)
            DataFrameAnalyzer.to_tsv_gz(next , "../../data/enrichment_results/20171125_enrichments_n" +
                                        str(nk) + "_by" + label + ".tsv.gz")
            DataFrameAnalyzer.to_tsv_gz(next[next['p.val.adj'] < 0.05] , "../../data/enrichment_results/20171125_enrichments_n" +
                                        str(nk) + "_by" + label + "_sig.tsv.gz")

        selected_labels = list(enrich_df[enrich_df['p.val.adj'] < 0.05].head(3)[label])
        if label == 'tf':
            for selected_i, next_selected in enumerate(selected_labels):

                print next_selected
                by_group = []

                df3 = df2[(df2[label + '1'] == next_selected) |
                          (df2[label + '2'] == next_selected)]

                print df3
                print tf_fam
                print 'here...'
                for cluster_i, grp in df3.groupby('cluster.id'):

                    print cluster_i
                    # print grp.head()

                    if label == 'tf':
                        for tf in tf_fam.keys():
                            # print tf
                            by_group.append([cluster_i, tf,
                                             sum([tf1 == tf or tf2 == tf
                                                  for tf1, tf2 in
                                                  zip(grp[label + '1'],
                                                      grp[label + '2'])])])

                            # print by_group[-1]

                by_group = pd.DataFrame(by_group, columns=['cluster', label, 'n'])

                print by_group[by_group[label] == 'FOXO1']

                by_group = by_group[[(tf_fam[k] if label == 'fam' else k) == next_selected
                                     for k in by_group[label]]]

                by_group = by_group.pivot(index='cluster', columns=label,
                                          values='n')

                by_group.columns = [c.decode("utf8") for c in by_group.columns]
                # print by_group
                for tf in by_group.columns:
                    if sum(by_group[tf]) == 0:
                        del by_group[tf]
                by_group_counts = pd.DataFrame([r for ri, r in by_group.iterrows()],
                                               index=by_group.index,
                                               columns=by_group.columns)
                for c in by_group_counts.columns:
                    by_group_counts[c] = [str(k) if k != 0 else ""
                                          for k in by_group_counts[c]]
                # by_group = by_group[[tf_name for tf_name in by_group.columns
                #                      if tf_name.startswith('FOX')]]
                # by_group_counts = by_group_counts[[tf_name for tf_name in by_group_counts.columns
                #                                    if tf_name.startswith('FOX')]]

                cbar_ax = fig.add_axes([0.91, .25, .02, .15])
                ax = plt.subplot2grid((12, 3),
                                      (7, selected_i), rowspan=5)

                print by_group
                sns.heatmap(by_group.reset_index(drop=True),
                            cmap='Greens', linecolor='white', linewidths=0.5,
                            annot=by_group.reset_index(drop=True),
                            fmt='', vmax=20, cbar_ax=cbar_ax)

                # ax.xaxis.tick_top()
                plt.xticks(rotation=45, fontsize=8.5, ha='right')
                plt.ylabel('cluster' if selected_i == 0 else '')
                plt.yticks(rotation=0)
                plt.xlabel(next_selected)

                plt.axhline(0, color='black', linewidth=2.0)
                plt.axhline(by_group.shape[0], color='black', linewidth=2.0)
                plt.axvline(0, color='black', linewidth=2.0)
                plt.axvline(by_group.shape[1], color='black', linewidth=2.0)
                cbar_ax.tick_params(labelsize=8)
                cbar_ax.set_ylabel("# datasets", ha='center', fontsize=12, rotation=90)

        else:

            # print len(by_group.columns)
            # print len(enrich_df.sort_values('p.val', ascending=True)['fam'])

            by_group = by_group[sorted(by_group.columns,
                                       key=lambda fam:
                                       min(enrich_df[enrich_df['fam'] == fam]['p.val']))]


            # before saving the top three, show all cases
            # plt.tight_layout()
            show_all = True
            if show_all:
                print i
                cbar_ax = fig.add_axes([0.91, .35, .02, .15])
                ax = plt.subplot2grid((12, 1),
                                      (5, 0), rowspan=5)
                # print next
                # print enrich_df[enrich_df['fam'] == next.columns[0]]
                next = by_group



                odds = None
                pvals = None
                n_df = None
                for fam in next.columns:
                    print fam
                    next_odds = enrich_df[enrich_df['fam'] == fam][['odds']].reset_index(drop=True)
                    next_pvals = -np.log10(enrich_df[enrich_df['fam'] == fam][['p.val.adj']]).reset_index(drop=True)
                    next_n = enrich_df[enrich_df['fam'] == fam][['a']].reset_index(drop=True)

                    if 'HMG' in fam:
                        fam = 'HMG'

                    if odds is None:
                        odds = next_odds
                        odds.columns = list(odds.columns)[:-1] + [fam]
                        print odds
                        pvals = next_pvals
                        pvals.columns = list(pvals.columns)[:-1] + [fam]

                        n_df = next_n
                        n_df.columns = list(n_df.columns)[:-1] + [fam]

                    else:
                        print next_odds
                        print odds.shape
                        print next_odds.shape
                        odds = pd.concat([odds, next_odds], axis=1)
                        odds.columns = list(odds.columns)[:-1] + [fam]

                        pvals = pd.concat([pvals, next_pvals], axis=1)
                        pvals.columns = list(pvals.columns)[:-1] + [fam]

                        n_df = pd.concat([n_df, next_n], axis=1)
                        n_df.columns = list(n_df.columns)[:-1] + [fam]


                # print pvals
                # print pvals[::-1]

                # print pvals
                # print odds
                # print n_df
                # print next
                # print 'OR:'
                odds_df = pd.DataFrame(odds)
                odds_df.index = range(nk)

                annot_asterisks = pd.DataFrame()
                for c in pvals:
                    annot_asterisks[c] =  RFacade.get_pval_asterisks([10 ** (-pi) for pi in pvals[c]])
                # print annot_asterisks
                odds_df.index = [int(idx) + 1 for idx in odds_df.index]
                sns.heatmap(odds_df, cmap='Reds', linecolor='white',
                            linewidths=0.5,
                            annot=annot_asterisks,
                            fmt='', cbar_kws={'label': 'odds ratio'},
                            vmin=0, vmax=12,
                            cbar_ax=cbar_ax,
                            ax=ax)
                plt.ylabel('cluster')
                plt.yticks(rotation=0)
                # plt.xticks(fonstize=4)

                cbar_ax = fig.add_axes([0.91, .6, .02, .15])
                ax = plt.subplot2grid((12, 1),
                                      (0, 0), rowspan=5)

                n_df.index = [int(idx) + 1 for idx in n_df.index]
                sns.heatmap(n_df, cmap='Blues', linecolor='white',
                            linewidths=0.5,
                            annot=n_df,
                            fmt='',
                            vmin=0, cbar_kws={'label': '# of datasets'},
                            cbar_ax=cbar_ax,
                            ax=ax)
                plt.xticks([])
                plt.ylabel('cluster')
                plt.yticks(rotation=0)



                plt.subplots_adjust(right=0.9, bottom=0.2, top=0.9, hspace=0.2)
                savefig(join("../../data/figures/fig3_clusters",
                             ylab + "_" + str(nki) + "_aggregated_models_all"),
                        dpi=1000, pdf=True)
                plt.close()

            by_group = by_group[by_group.columns[:3]]

            # print enrich_df[enrich_df['fam'] == 'Forkhead']

            for i in range(3):
                cbar_ax = fig.add_axes([0.91, .55, .02, .15])


                ax = plt.subplot2grid((12, 9),
                                      (0, i * 3), rowspan=5)
                next = by_group[[ci for j, ci in enumerate(by_group.columns)
                                 if j == i]]

                # print i
                # print next
                # print enrich_df[enrich_df['fam'] == next.columns[0]]

                next_fam = next.columns[0]
                # print next_fam

                # print -np.log10(enrich_df[enrich_df['fam'] == next.columns[0]]['p.val'])
                pvals = -np.log10(enrich_df[enrich_df['fam'] == next_fam]['p.val.adj'])

                # print enrich_df[enrich_df['fam'] == next_fam]
                odds = enrich_df[enrich_df['fam'] == next_fam]['odds']
                # print pvals
                # print pvals[::-1]

                # print next
                # print 'OR:'
                odds_df = pd.DataFrame(odds)
                odds_df.index = range(nk)

                annot_asterisks = pd.DataFrame(RFacade.get_pval_asterisks([10 ** (-pi)
                                                                           for pi in pvals]))
                # print annot_asterisks
                odds_df.index = [int(idx) + 1 for idx in odds_df.index]
                sns.heatmap(odds_df, cmap='Reds', linecolor='white',
                            linewidths=0.5,
                            annot=annot_asterisks,
                            fmt='',
                            vmin=0, vmax=20,
                            cbar_ax=cbar_ax,
                            ax=ax)

                ax.xaxis.tick_top()
                ax.xaxis.set_label_position('top')
                plt.xticks([])

                plt.title(tf_fam, x=0.5)
                # if i == 1:
                plt.title(next.columns[0])

                # print "matrix sum:", sum(by_group.sum())
                # print 'df length:', clusters_df.shape[0]
                plt.axhline(0, color='black', linewidth=2.0)
                plt.axhline(nk, color='black', linewidth=2.0)
                plt.axvline(0, color='black', linewidth=2.0)
                plt.axvline(next.shape[1], color='black', linewidth=2.0)
                plt.xticks(rotation=45, fontsize=8.5, ha='left')

                plt.yticks(rotation=0)
                # plt.yticks([], rotation=0)

                cbar_ax.tick_params(labelsize=8)
                cbar_ax.set_ylabel("OR", ha='center', fontsize=12, rotation=90)

                ax = plt.subplot2grid((12, 9),
                                      (0, i * 3 + 1), rowspan=5, colspan=1)

                ax.barh(range(nk), pvals[::-1])

                plt.xticks([0, 4, 8])
                plt.yticks([])
                # plt.ylim(-1, nk - 1)

                plt.xlabel('-log(p.adj)', fontsize=8)
                ax.spines['top'].set_visible(False)
                ax.spines['right'].set_visible(False)

    # plt.tight_layout()
    plt.subplots_adjust(right=0.9, bottom=0.2, top=0.7, hspace=0.2)
    savefig(join("../../data/figures/fig3_clusters", ylab + "_" + str(nki) + "_aggregated_models"),
            dpi=1000, pdf=True)
    plt.close()
print 'here...'

stop()
# count the number of significant evetns by both family and fam, to decide NK
enr_dir = '/g/scb2/zaugg/rio/EclipseProjects/zaugglab/comb-TF-binding/cap_selex_quantitative_analysis/data/enrichment_results'
padj_thr = 10
res = []
for k in range(2, 8):
    all_k = []
    for f in listdir(enr_dir):
        if 'sig' in f or not 'n' + str(k) in f:
            continue
        print f
        if f.endswith('.xlsx'):
            continue
        df = DataFrameAnalyzer.read_tsv_gz(join(enr_dir, f))
        if not 'sig' in f and f.endswith('.tsv.gz'):
            df.to_excel(join(enr_dir, f.replace(".tsv.gz", '.xlsx')))
        all_k.append(df)
    all_k = pd.concat(all_k)
    print k, all_k[all_k['p.val.adj'] < padj_thr / 100.0].shape[0]
    # res.append([k, all_k[all_k['p.val.adj'] < padj_thr / 100.0].shape[0]])
    res.append([k, all_k[all_k['p.val'] < 0.05].shape[0] / float(k)]) # padj_thr / 100.0].shape[0] / float(k)])
    # print k, all_k[all_k['p.val.adj'] < .11]
res = pd.DataFrame(res, columns=['nk', 'n'])
ax = plt.subplot()
plt.plot(res['nk'], res['n'])
plt.xlabel('# PAM clusters')
plt.ylabel('# associations')
plt.title('TF and TF family association\n to PAMclusters')
plt.subplots_adjust(left=0.7, top=.3)
plt.xticks([2, 3, 4, 5, 6])
remove_top_n_right_ticks(ax)
savepdf('../../data/figures/n_vs_nk')
plt.close()



sns.set_style('white')
sel = df.copy()
sel['mean'] = sel.mean(axis=1)
sel['cluster.id'] = n_medoids['n.2']
sel = sel.sort_values(['cluster.id', 'mean'], ascending=[True, False])
del sel['mean']
del sel['cluster.id']
max_perc = max(df.mean())
hm1_ax = sns.heatmap(sel, cmap='Greys', vmin=0, vmax=max_perc,
                     cbar_kws={'label': '$\Delta$' + 'R' + '$^2$',
                               'orientation': 'horizontal'},
                     cbar_ax=cbar_ax)

cbar_ax.set_xlabel('$\Delta$' + 'R' + '$^2$', fontsize=12)
plt.xticks([0, 500, 999], ['0', '0.5', '1.0'], rotation=0)
plt.xlabel('Relative motif position')
plt.yticks([], rotation=0, fontsize=3)
plt.title("medoids")
plt.ylabel("CAP-SELEX datasets")
# debug
# plt.show()
plt.subplots_adjust(wspace=0.5)
savefig(join("../../data/figures/fig3_clusters/raw"),
        pdf=False, png=True, dpi=100)
plt.close()


print n_medoids.shape

dpi = 1000
for nk in range(2, 11):
    # if nk != 2:
    #     continue

    print 'here...'

    # we are going to plot only the # of clusters when k = 6
    cluster_ids = list(n_medoids['n.' + str(nk)])
    clusters_df = pd.DataFrame(cluster_ids, columns=['cluster.id'])


    # save distributions of lengths in each cluster

    # df['median.r2'] = df.median(axis=1)
    df['median.r2'] = df.mean(axis=1)

    df2 = pd.concat([clusters_df.reset_index(drop=True), df.reset_index(drop=True)], axis=1)
    print df2.head()

    # df2 = df2.sort_values('median.r2', ascending=False)

    # important step: renumber cluster id from 1 to five according to the mean signal (R2)
    med_signal_cluster = {ci: median(grp['median.r2']) for ci, grp in df2.groupby('cluster.id')}
    df2['med.r2.cluster'] = [med_signal_cluster[ci] for ci in df2['cluster.id']]

    df2 = pd.concat(df2[df2['cluster.id'] == ci].sort_values('median.r2', ascending=False)
                    for ci in sorted(med_signal_cluster, key = lambda x: med_signal_cluster[x]))

    df2['mean'] = df2[[c for c in df2 if not c in {'cluster.id', 'median.r2', 'median.r2.cluster', 'rank'}]].mean(axis=1)
    df2 = df2.sort_values(['med.r2.cluster', 'mean'], ascending=[False, False])
    df2['rank'] = df2['med.r2.cluster'].rank(method='dense', ascending=True)
    df2['cluster.id'] = df2['rank'].astype(int)

    del df2['median.r2']
    del df2['med.r2.cluster']
    del df2['rank']
    del df2['mean']

    # these are the features weights
    df2_w = pd.concat([clusters_df, df_r2], axis=1)
    print df2_w

    # we are not sorting the weights yet

    last_value = None
    boundaries = []
    df2 = df2.reset_index(drop=True)
    for ri, r in df2.iterrows():
        curr_value = r['cluster.id']
        if last_value is None or last_value != curr_value:
            last_value = curr_value

            # boundaries.append(df.shape[0] - ri)
            boundaries.append(ri)
        last_value = curr_value
    print boundaries
    fig = plt.figure(figsize=(14, 8))
    plt.subplots_adjust(hspace=0.0, top=0.75, wspace=0.2)

    cbar_ax = fig.add_axes([0.15, .9, .1, .010])
    hm1_ax = plt.subplot(1, 4, 1)

    # the max to be shown is the max across the mean of all clusters
    max_perc_options = []
    for nki in range(nk):
        sel = df2[df2['cluster.id'] == nki + 1]
        max_perc_options.append(max(list(sel.mean())))

    max_perc = max(max_perc_options)

    sns.set_style('white')
    # if 'cluster.id' in df2:
    #     del df2['cluster.id']
    hm1_ax = sns.heatmap(df2[[c for c in df2 if str(c) != 'cluster.id']], cmap='Greys',  vmin=0, vmax=max_perc, ax=hm1_ax,
                cbar_kws={'label': '$\Delta$' + 'R' + '$^2$',
                          'orientation': 'horizontal'},
                cbar_ax=cbar_ax)



    cbar_ax.set_xlabel('$\Delta$' + 'R' + '$^2$', fontsize=12)
    plt.xticks([0, 500, 999], ['0', '0.5', '1.0'], rotation=0)
    plt.xlabel('Relative motif position')
    plt.yticks([], rotation=0, fontsize=3)
    plt.title("medoids")
    plt.ylabel("CAP-SELEX datasets")

    for bi in boundaries:
        plt.axhline(bi, linewidth=3.0, color='Red')


    # debug
    # plt.show()
    # plt.subplots_adjust(wspace=0.5)
    # savefig(join("../../data/figures/fig3_clusters", str(nk) + "_aggregated_models"),
    #         pdf=False, png=True, dpi=dpi)
    # plt.close()
    # assert 1 > 2



    cbar_ax2 = fig.add_axes([0.55, .9, .1, .01])

    hm2_ax = plt.subplot(1, 4, 3)

    sns.set_style('white')

    sns.heatmap(df2_w, cmap='Blues',  vmin=0, vmax=1.0, ax=hm2_ax, cbar_kws={'label': 'dR^2\n(normed)',
                                                                       'orientation': 'horizontal'},
                cbar_ax=cbar_ax2)
    plt.subplots_adjust(hspace=0.0, top=0.75, wspace=0.2)

    plt.xticks([])
    plt.yticks([], rotation=0, fontsize=3)
    plt.title('%R2 changes')
    # plt.tight_layout()


    for bi in boundaries:
        plt.axhline(bi, linewidth=3.0, color='Red')

    print set(df2['cluster.id'])
    from matplotlib.patches import ConnectionPatch

    sns.set_style('ticks')
    for nki in range(nk):
        sel = df2[df2['cluster.id'] == nki + 1]
        del sel['cluster.id']
        print nki, sel.shape[0]
        ax = plt.subplot(nk, 4, nk * 4 - 4 * nki - 2)
        plt.plot(range(sel.shape[1]), list(sel.mean()))
        ax.fill_between(range(sel.shape[1]),
                         list(sel.mean() - sel.std()),
                         list(sel.mean() + sel.std()), alpha=0.2)
        print nki
        # print list(sel.mean())
        plt.xlim([0, 1000])
        from math import ceil
        ymax = ceil(max_perc)
        plt.ylim([0, ymax])
        plt.yticks([0, ymax / 2.0])
        plt.xticks([])
        plt.axvline(500, linestyle='--', color='gray')
        # if nki == 2:
        #     plt.ylabel('Aggregated signal')

        if nki == 0:
            plt.xticks([0, 500, 999], ['0', '0.5', '1.0'], rotation=0)
            plt.xlabel('Relative motif position')

        #print boundaries[nki]

        # add the top line
        if nki + 1 == nk:
            con = ConnectionPatch((0, ymax), (999, 0), coordsA="data",
                                  coordsB="data",
                                  axesA=ax, axesB=hm1_ax, linestyle='--',
                                  color='gray')
            ax.add_artist(con)

        # add the next line
        if nki != 0:
            print nki
            con = ConnectionPatch((0, 0), (999, boundaries[nk - (nki)]), coordsA="data",
                                  coordsB="data",
                                  axesA=ax, axesB=hm1_ax, linestyle='--',
                                  color='gray')
            ax.add_artist(con)
        else:
            con = ConnectionPatch((0, 0), (999, df2.shape[0]), coordsA="data",
                                  coordsB="data",
                                  axesA=ax, axesB=hm1_ax, linestyle='--',
                                  color='gray')
            ax.add_artist(con)


    del df2['cluster.id']

    for nki in range(nk):
        sel = df2_w[df2_w['cluster.id'] == nki + 1]
        del sel['cluster.id']
        print nki, sel.shape[0]
        ax = plt.subplot(nk, 4, nk * 4 - 4 * nki)
        plt.plot(range(sel.shape[1]), list(sel.mean()))

        ax.fill_between(range(sel.shape[1]),
                         list(sel.mean() - sel.std()),
                         list(sel.mean() + sel.std()), alpha=0.2)

        plt.axvline(500, linestyle='--', color='gray')
        # if nki == 2:
        #     plt.ylabel('Aggregated signal')

        # plt.ylim([0, 1])
        plt.yticks([.25, .5])
        plt.xticks([])


    # plt.show()
    plt.subplots_adjust(wspace=0.5)
    savefig(join("../../data/figures/fig3_clusters", str(nk) + "_aggregated_models"),
            pdf=False, png=True, dpi=dpi)
    plt.close()

stop()
# exit()
cg = sns.clustermap(df, col_cluster=False, cmap='Reds', standard_scale=1,
                    method='average')
plt.setp(cg.ax_heatmap.yaxis.get_majorticklabels(), rotation=0, fontsize=4)
plt.setp(cg.ax_heatmap.xaxis.get_majorticklabels(), rotation=0, fontsize=0)
plt.show()
if not exists(shape_profiles_bkp):
    # calculate_correlations
    table = []
    dists = [[0 for i in range(len(ids))] for j in range(len(ids))]
    pos_by_id = {id: i for i, id in enumerate(ids)}
    curr_i = None
    for id1, id2 in combinations(ids, 2):
        a, b = averages_by_id[id1], averages_by_id[id2]

        print len(a), a
        print len(b), b
        exit()