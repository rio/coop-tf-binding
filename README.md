![alt-text-1](./icon/icon.jpg "title-1")

![alt-text-2](./icon/cloud_all.jpg "title-2")
[**Cloud App available in Heroku (Click here)**](https://coop-tf-binding.herokuapp.com/)

The paper has been recommended in Faculty Opinions.

<a href="https://f1000.com/prime/737174349" target="_blank"><img src="https://blog.f1000.com/wp-content/uploads/2020/04/Tertiary-logo@2x-3.png" alt="Access recommendation" id="bg" width="280px" height="45px" /></a>

Source code related to
#### **Mechanistic insights into transcription factor cooperativity and its impact on protein-phenotype interactions**
---------------------------------------------------------
**Authors**: Ignacio L. Ibarra, Nele M. Hollmann, Bernd Klaus, Sandra Augsten, Britta Velten, Janosch Hennig, and Judith B. Zaugg
### Usage
----------------
1. Scripts allow the processing of SELEX input data studied in this manuscript (**examples 1 to 4**).
2. Scripts also allow the calculation of Z-scores between between TF-pairs and ontologies, using peak data and TF-TF *k*-mers of interest (**example 5**).
3. Examples are provided based on own peak data collected from multiple sources. Once executed, you can also interrogate your own data by replacement of the input dataframes.
4. Please refer to the [manuscript's Methods](link) for details on the source of the files.

### Requirements
- Programming knowledge in Python and Data Science skills are required to run these examples.
- See Installation to get this code running.
- See Relevant examples below.

### Data
----------------
- Preprocessed data can be found here. http://www.zaugg.embl.de/data-and-tools/coop-tf-binding/

### Dependencies
----------------
- [Python 2.7](https://www.python.org/download/).
- `python` dependencies:
    * `pandas numpy seaborn scikit-learn` (not all scripts require those. processing and plotting)
    * Scripts require the following dependencies library https://git.embl.de/rio/wild

### Installation (typical installation time: around 5-10 minutes)
----------------
1) Download and add to your libraries the following library code [`wild`](https://git.embl.de/rio/wild). This library contains methods/classes invoked by the example scripts.
2) Add the `wild` library to your `python` environment (e.g. modifying `PYTHONPATH`, using `pip`, or with another option).
3) Download the code provided in this repository, with one of these two commands.

```
# option 1
git clone https://git.embl.de/rio/coop-tf-binding.git

# option 2
git clone git@git.embl.de:rio/coop-tf-binding.git
```

4) Go to the directory that contains the script you want to run, and execute `python script_name.py`. E.g. for the example number 5.1:

```
cd combinatorial_binding_analysis_invivo/src/12_great_scan_debug
python wp_1_test_peaks_n_kmers_vs_ontologies.py
```
### Relevant examples
----------------------
1) Calculation of *k*-mer relative affinities from SELEX data.
[#1.1](https://git.embl.de/rio/coop-tf-binding/blob/master/cap_selex_quantitative_analysis/src/tiled_kmers_analysis/wp_0_test_calculate_relative_affinities.py)
2) Calculation of trim-and-summarize coefficients of determination, from TF-binding models.
[#2.1](https://git.embl.de/rio/coop-tf-binding/blob/master/cap_selex_quantitative_analysis/src/tiled_kmers_analysis/wp_2_assess_improvements_per_position_capselex.py)
[#2.2](https://git.embl.de/rio/coop-tf-binding/blob/master/cap_selex_quantitative_analysis/src/tiled_kmers_analysis/wp_3_1_load_and_plot_global_improvements.py)
3) Cooperative binding sites scoring
[#3.1](https://git.embl.de/rio/coop-tf-binding/blob/master/cap_selex_quantitative_analysis/src/09_forkhead_ets_perf_bias_assessment/wp_5_calculate_cooperativity_cap_FOXO1_ELK3.py)
4) Shape profiles
[#4.1](https://git.embl.de/rio/coop-tf-binding/blob/master/cap_selex_quantitative_analysis/src/03_feature_clustering/wp_3_5_cluster_by_interpolation_aggregated_models.py)
5) TF-TF association to ontologies (Z-scores)
[#5.1](https://git.embl.de/rio/coop-tf-binding/blob/master/combinatorial_binding_analysis_invivo/src/12_great_scan_debug/wp_1_test_peaks_n_kmers_vs_ontologies.py)
[#5.2](https://git.embl.de/rio/coop-tf-binding/blob/master/combinatorial_binding_analysis_invivo/src/12_great_scan_debug/wp_5_calculate_aurocs.py)

### Errors, bugs or further questions
----------------------
Please report to ignacio.ibarra@embl.de (or in Twitter [@ilibarra](https://twitter.com/ilibarra)). We will allow the `Issues` page soon and we will move all collected feedback there.

### Citation
----------------------
If you want to cite this work please refer to. 
**Ibarra *et al*. (2020) Mechanistic insights into transcription factor cooperativity and its impact on protein-phenotype interactions. *Nature Communications*** 
