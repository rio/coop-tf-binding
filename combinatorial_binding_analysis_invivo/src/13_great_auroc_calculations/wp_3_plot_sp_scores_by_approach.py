'''
Created on 4/7/2019

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.plot_utils import *
from lib.GOCombinatorialBinding import GOCombinatorialBinding

for ontology_id in ['HPO', 'DISEASES', 'GO']:
    if ontology_id != 'HPO':
        continue
    for pi, label in enumerate(['tf1.and.tf2', 'tf1.or.tf2', 'both']):
        if label != 'tf1.and.tf2':
            continue
        all = []
        for approach in ['peaks+kmers', 'peaks', 'kmers']:
            p = '../../data/GREAT_zscores_downsampling_v2019/%s_%s_%s.tsv.gz' % (ontology_id, label, approach)
            res = DataFrameAnalyzer.read_tsv_gz(p)
            print ontology_id, label, approach, res.shape[0]

            print res.shape

            if label == 'tf1.and.tf2':
                pos = res[res[label]]
                neg = res[~res[label] & ~res['tf1.or.tf2']]
            elif label == 'tf1.or.tf2':
                pos = res[res[label] & ~res['tf1.and.tf2']]
                neg = res[~res[label]]
            pos['label'] = label
            neg['label'] = 'background'
            print approach, pos.shape, neg.shape

            next = pd.concat([pos, neg])
            next['approach'] = approach
            all.append(next)

        all = pd.concat(all)
        # sns.violinplot(data=all, y='sp.score', x='approach', hue='label', # order=[True, False],
        #                # showfliers=False, zorder=0,
        #                width=1.2, inner='quartile', split=True, hue_order=[True, False], palette={True: 'red', False: 'gray'},
        #                scale_hue=False, linewidth=0.2)
        ax = plt.subplot()
        sns.boxplot(data=all, y='sp.score', x='label', hue='approach',
                    order=[label, 'background'],
                    hue_order=['peaks+kmers', 'peaks', 'kmers'], showfliers=False, zorder=1, width=.65,
                    palette={'peaks+kmers': '#E21E25', 'peaks': '#377EB8', 'kmers': '#4DAE48'})
        plt.title(approach)

        stop()
        plt.ylim([0, .2 if label == 'tf1.and.tf2' else .7])
        remove_top_n_right_ticks(ax)
        plt.setp(ax.artists, edgecolor='black', linewidth=0.35)
        plt.setp(ax.lines, color='black', linewidth=0.35)
        plt.subplots_adjust(right=0.4, top=0.6)

        from matplotlib.ticker import FormatStrFormatter
        plt.yticks([0.0, 0.05, 0.1, 0.15, 0.2])
        ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
        savepdf("../../data/figures/%s_%s_boxplot" % (ontology_id, label))
        plt.close()