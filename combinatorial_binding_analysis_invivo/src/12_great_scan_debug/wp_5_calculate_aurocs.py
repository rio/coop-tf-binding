'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *

'''
Created on 

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.plot_utils import *
from lib.DISEASES import DISEASES
from lib.RFacade import RFacade
from lib.ThreadingUtils import ThreadingUtils
from lib.OntologiesAnalyzer import OntologiesAnalyzer


def calculate_aurocs_query(jobid=None, **kwargs):

    ontology_ids = ['DISEASES', 'HPO', 'GO']

    queries = [[ont, i] for ont in ontology_ids for i in range(15)]

    for q in queries:
        print q

    for next_jobi, q in enumerate(queries):
        if next_jobi != jobid:
            continue
        print next_jobi, q

        ontology_id, label_feats = q

        # INPUT DATA available at
        # https://www.embl.de/download/zaugg/rio/OAP_all.tar.gz
        # please downlaod uncompress content in this directory (../../data/GREAT_zscores_downsampling_all_remap_ids/)
        basedir = '../../data/GREAT_zscores_downsampling_all_remap_ids/%s' % ontology_id
        res = []
        paths = [join(basedir, f) for f in listdir(basedir)]

        from multiprocessing import Manager
        manager = multiprocessing.Manager()
        return_dict = manager.dict()

        def get_res(p, pi):
            f = basename(p)
            df = DataFrameAnalyzer.read_tsv_gz(p)
            no_motif = df[np.isnan(df['thr'])]
            # df = df[(df['binom.pval'] < 0.np.isnan(05) & (df['z.score.genes'] > 3))]
            df = df[~np.isnan(df['thr'])]
            df = df[~np.isinf(df['z.score.genes'])]
            print f, df.shape[0]
            df['filename'] = f
            df['minus.log10.pval'] = -np.log10(df['binom.pval'])
            df['x'] = df['minus.log10.pval']
            df['y'] = df['z.score.genes']

            df = df[~np.isnan(df['x'])]
            df = df[~np.isnan(df['y'])]

            # df['z'] = (df['x'] ** 2) + (df['y'] ** 2)
            df = df.sort_values('z.score.genes', ascending=False).drop_duplicates('id')
            # df['z'] = np.where(df['y'] < 0, 0, df['z'])
            # df['rank'] = df['z'].rank(ascending=False)
            next = no_motif[['id', 'name', 'binom.pval',
                             'z.score.genes', 'z.score.peaks']].merge(df[['id', 'name', 'binom.pval',
                                                                          'z.score.genes', 'z.score.peaks']], on='id')
            next['tf.pair'] = f.replace(".tsv.gz", '')
            return_dict[pi] = next

        N = len(paths)
        ngroup = 10
        output_list = [None for i in range(len(paths[:N]))]
        ThreadingUtils.run(get_res, [[paths[pi], pi] for pi in range(len(paths[:N]))], ngroup)
        res = pd.concat([return_dict[k] for k in return_dict.keys()])

        res['tf1'] = res['tf.pair'].str.split("_").str[0]
        res['tf2'] = res['tf.pair'].str.split("_").str[1]


        print 'reading ontologies'
        ontologies = OntologiesAnalyzer.get_gos()
        sel_ontology = ontologies[0][ontologies[0]['ontology.group'] == ontology_id]

        tfs_by_term = {goid + "_" + g for goid, grp in sel_ontology.groupby('id') for g in set(grp['gene.name'])}
        res['tf1.found'] = (res['id'] + "_" + res['tf1']).isin(tfs_by_term)
        res['tf2.found'] = (res['id'] + "_" + res['tf2']).isin(tfs_by_term)
        res['tf1.or.tf2'] = res['tf1.found'] | res['tf2.found']
        res['tf1.and.tf2'] = res['tf1.found'] & res['tf2.found']

        # res = res[(res['binom.pval_x'] < 0.05) & (res['binom.pval_y'] < 0.05)]
        res['z.score.genes_x'] = np.where(np.isnan(res['z.score.genes_x']) | np.isinf(res['z.score.genes_x']), 0, res['z.score.genes_x'])
        res['z.score.genes_y'] = np.where(np.isnan(res['z.score.genes_y']) | np.isinf(res['z.score.genes_y']), 0, res['z.score.genes_y'])

        res['z.score.peaks_x'] = np.where(np.isnan(res['z.score.peaks_x']) | np.isinf(res['z.score.peaks_x']), 0, res['z.score.peaks_x'])
        res['z.score.peaks_y'] = np.where(np.isnan(res['z.score.peaks_y']) | np.isinf(res['z.score.peaks_y']), 0, res['z.score.peaks_y'])


        query_ids = set(ontologies[0][ontologies[0]['id'].str.contains('DOID')]['id'])
        n_by_id = {k: v for k, v in ontologies[0]['id'].value_counts().iteritems()}
        genes_by_id = {k: set(grp['gene.name']) for k, grp in ontologies[0].groupby('id')}

        n_genes_cutoff = 2000
        sel = res[(res['id'].map(n_by_id) < n_genes_cutoff)] # (res['id'].map(n_by_id) > 10)]

        # sel = sel[(sel['binom.pval_x'] < 0.1) & (sel['binom.pval_y'] < 0.1)]

        from lib.MachineLearning import MachineLearning
        from sklearn.metrics import precision_recall_curve

        lw = 2
        aurocs = []
        auprcs = []

        for label_y in ['tf1.or.tf2', 'tf1.and.tf2']:
            if label_y != 'tf1.and.tf2':
                continue
            pos = sel[sel[label_y]]
            neg = sel[~sel['tf1.or.tf2'] &  ~sel['tf1.and.tf2']].sample(int(pos.shape[0] / .1))
            # neg = sel[~sel[label_y]].sample(int(pos.shape[0] / .1))
            next = pd.concat([pos, neg]).reset_index(drop=True)
            y_true = next[label_y]

            assert sum(np.isinf(y_true)) == 0 and sum(np.isnan(y_true)) == 0


            from sklearn import svm
            from sklearn.multiclass import OneVsRestClassifier

            print pos.shape, neg.shape, next.shape

    #         for X in [next[['z.score.genes_x', 'z.score.peaks_x']].values,
    #                   next[['z.score.genes_x', 'z.score.genes_y', 'z.score.peaks_x', 'z.score.peaks_y']].values]:
            features = ['z.score.genes_x', 'z.score.genes_y', 'z.score.peaks_x', 'z.score.peaks_y']
            feature_combinations = [c for k in range(1, 5) for c in combinations(features, k)]
            id_combinations = [c for k in range(1, 5) for c in combinations([0, 1, 2, 3], k)]

            for label_combinations_i, next_labels in enumerate(id_combinations):

                if label_combinations_i != label_feats and label_combinations_i not in {0, 4}:
                    continue

                label_str = "".join(map(str, [next_k for next_k in next_labels]))
                output_dir = '../../data/figures/GREAT_aurocs'
                output_path = join(output_dir, 'train_test_auroc_%s_%s_%s.tsv.gz' % (ontology_id, label_str, label_y))

                X = next[[features[next_id] for next_id in next_labels]].values
                features_k = [features[k].replace(".score", '') for k in next_labels]

                auroc, auprc = MachineLearning.get_auroc_n_auprc(list(y_true), list(X.max(axis=1)))

                print auroc, auprc

                from sklearn.metrics import roc_curve, auc
                X, y_true = np.array(X), np.array(y_true)
                print 'fitting classifier with # of features...', X.shape[1]

                from lib.MachineLearning import MachineLearning
                kfold_res = MachineLearning.KFoldSVC(X, y_true)

                kfold_res['feat.k'] = label_str

                print label_combinations_i, median(kfold_res['auroc.test'])

                DataFrameAnalyzer.to_tsv_gz(kfold_res, output_path)

                random_state = np.random.RandomState(0)
                classifier = OneVsRestClassifier(svm.LinearSVC(random_state=random_state))
                clf = classifier.fit(X, y_true)
                y_pred = clf.decision_function(X)
                from sklearn.metrics import roc_curve, auc

                auroc, auprc = MachineLearning.get_auroc_n_auprc(y_true, y_pred)

                aurocs.append(auroc)
                auprcs.append(auprc)


                plot = True
                if plot:
                    plt.subplot(1, 2, 1)
                    fpr, tpr, _ = roc_curve(y_true, y_pred)
                    plt.plot(fpr, tpr)
                    plt.subplot(1, 2, 2)
                    precision, recall, _ = precision_recall_curve(y_true, y_pred)
                    plt.plot(recall, precision)

                    print auc(fpr, tpr)

                    plt.subplot(1, 2, 1)
                    plt.legend(['peaks (single association): %.2f' % aurocs[0]],
                                # 'peaks+kmers (single association): %.2f' % aurocs[1],
                                # 'peaks (double association): %.2f' % aurocs[2],
                                # 'peaks+kmers (double association): %.2f' % aurocs[3]],
                               loc="lower right", fontsize=7)
                    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
                    plt.xlabel('False Positive Rate')
                    plt.ylabel('True Positive Rate')
                    plt.title('ROC curve')
                    plt.subplot(1, 2, 2)
                    plt.legend(['peaks (single association): %.2f' % auprcs[0]],
                                # 'peaks+kmers (single association): %.2f' % auprcs[1],
                                # 'peaks (double association): %.2f' % auprcs[2],
                                # 'peaks+kmers (double association): %.2f' % auprcs[3]],
                               loc="upper right", fontsize=7)
                    plt.xlabel('Recall')
                    plt.ylabel('Precision')
                    plt.title('PR-curve')
                    plt.subplots_adjust(bottom=0.5)

                    savepng(join(output_dir, ontology_id + "_" + label_str + "_"+ str(int(X.shape[1])) + "_" + str(label_y)))
                    plt.close()
                    # save values
                    DataFrameAnalyzer.to_pickle([fpr, tpr, recall, precision, auroc, auprc],
                                                join(output_dir, ontology_id + "_" + label_str + "_" +
                                                     str(int(X.shape[1])) + "_" + str(label_y)) + "_plot_values.pkl")


                plot_network_data = False
                if plot_newtwork_data:
                    print auprcs

                    # network selection
                    z_score_thr = 5
                    net = sel[(sel['z.score.genes_x'] > z_score_thr) & (sel['z.score.genes_y'] > z_score_thr) & sel['tf1.or.tf2']]
                    tf_names = set(net['tf1']).union(set(net['tf2']))
                    n_hits_by_tf = {tf: net[(net['tf.pair'].str.startswith(tf + "_")) | (net['tf.pair'].str.endswith("_" + tf))].drop_duplicates('tf.pair').shape[0] for tf in tf_names}
                    net['n.edges.tf1'] = net['tf1'].map(n_hits_by_tf)
                    net['n.edges.tf2'] = net['tf2'].map(n_hits_by_tf)
                    from scipy.stats import gmean
                    net['z.score.genes.avr'] = gmean(np.array(net[['z.score.genes_x', 'z.score.genes_y']]), axis=1)
                    net['minus.log.10.binom.pval_y'] = -np.log10(net['binom.pval_y'])
                    net = net.drop_duplicates('tf.pair')
                    first_cols = ['tf1', 'tf2', 'n.edges.tf1', 'n.edges.tf2']
                    net = net[first_cols + [c for c in net.columns if not c in set(first_cols)]]
                    net = net[net['binom.pval_y'] < 0.05]

                    if ontology_id == 'DISEASES':
                        print 'setting confidences tf1...'
                        net['confidence.tf1'] = [list(sel_ontology[(sel_ontology['id'] == r['id']) &
                                                                   (sel_ontology['gene.name'] == r['tf1'])]['confidence'])
                                                 for ri, r in net.iterrows()]
                        print 'setting confidences tf2...'
                        net['confidence.tf2'] = [list(sel_ontology[(sel_ontology['id'] == r['id']) & (sel_ontology['gene.name'] == r['tf2'])]['confidence'])
                                                 for ri, r in net.iterrows()]

                        net['confidence.tf1'] = np.where(net['confidence.tf1'].astype(str).str.len() == 0, np.nan, net['confidence.tf1'].astype(str).str[0])
                        net['confidence.tf2'] = np.where(net['confidence.tf2'].astype(str).str.len() == 0, np.nan, net['confidence.tf2'].astype(str).str[0])
                        net['best.confidence'] = net[['confidence.tf1', 'confidence.tf2']].max(axis=1)
                    DataFrameAnalyzer.to_tsv(net, join(output_dir, "network_auroc_combinatorial_binding_invivo_%s_%s.tsv" % (ontology_id, label_y)))


if __name__ == '__main__':
    BSubFacade.prepare_jobarray_scripts(__file__, calculate_aurocs_query, n_jobs=45, n_jobs_max=30, ncores=10,
                                        output_dir='../../output/bsub_output', queue='htc',
                                        mem=7000, qos='high', time='02:00:00')