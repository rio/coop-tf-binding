'''
Created on

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.REMAPAnalyzer import REMAPAnalyzer
from lib.GOCombinatorialBinding import GOCombinatorialBinding
from lib.GREATAnalyzer import GREATAnalyzer
from lib.OntologiesAnalyzer import OntologiesAnalyzer
from lib.SELEX.CAPSELEXAnalyzer import CAPSELEXAnalyzer
from lib.SequenceMethods import SequenceMethods
from lib.DISEASES import DISEASES
from lib.FastaAnalyzer import FastaAnalyzer
from lib.GREATAnalyzer import GREATAnalyzer


def run(jobid=None, **kwargs):


    query = kwargs.get('query', None)

    # just focus in DISEASE-TF combinations that are found in DISEASES+REMAP, together
    output_dir = '../../data/GREAT_zscores_downsampling_v2019'
    if not exists(output_dir):
        mkdir(output_dir)

    subset_cap_tfs = None
    if query is not None:
        q_tf1, q_tf2 = query.split("_")[:2]
        subset_cap_tfs = {q_tf1, q_tf2}

    queries_df = kwargs.get('queries_df', GOCombinatorialBinding.get_queries_df(overwrite=False, subset_cap_tfs=subset_cap_tfs))


    user_query_ids = kwargs.get('user_defined_go_ids', None)

    user_query_ids = {'GO:0031424', 'GO:0021953', 'GO:0007156', 'GO:0030182'}
    # if user_query_ids is not None and isinstance(user_query_ids, str):
    #     print user_query_ids
    #     exit()

    import time
    import datetime
    x = time.strptime(kwargs.get('time', '01:00:00'), '%H:%M:%S')
    timemin = datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds() / 60.0

    print ''
    print 'total time to run (in min.):', timemin


    # save supp table with all models that are potentially tested
    queries_df.head()
    supp = queries_df[['tf.cap.1', 'tf.remap.1', 'fam.cap.1', 'fam.remap.1', 'tf.cap.2', 'tf.remap.2', 'fam.cap.2', 'fam.remap.2', 'motif']]
    supp['k'] = ["_".join(map(str, r.values)) for ri, r in supp.iterrows()]
    supp = supp.drop_duplicates('k')
    supp['k'] = ["_".join(map(str, r.values)) for ri, r in supp[[c for c in supp if c not in {'k', 'motif'}]].iterrows()]
    del supp['motif']
    supp = supp.drop_duplicates('k')
    for c in supp:
        supp = supp[~pd.isnull(supp[c])]

    supp = supp[['tf.remap.1', 'fam.remap.1', 'tf.remap.2', 'fam.remap.2']]
    supp['k'] = ["_".join(r.values) for ri, r in supp.iterrows()]
    supp = supp.drop_duplicates('k')
    del supp['k']
    supp = supp.reset_index(drop=True)
    supp.to_excel('../../data/supp_XX_queries_OAP_partial_and_full_matches.xlsx', index=None)
    stop()
    n_missing = 0

    ngroup = kwargs.get('ngroup', 50)
    start = jobid * ngroup
    end = (jobid + 1) * ngroup

    print 'indexes to focus on: from', start, 'to', end

    # print "# of queries (total, across all ontologies)", len(pairs)
    print "# of queries (peaks only)", queries_df[queries_df['model'] == 'peaks'].shape[0]

    print 'loading ontologies...'
    ontologies = OntologiesAnalyzer.get_gos()
    tfs_by_term_and_ontology = {}
    for ontology_id in ['DISEASES', 'HPO', 'GO']:
        print ontology_id
        # if ontology_id != 'GO':
        # obo = OntologiesAnalyzer.get_obos_by_ontology(ontology_id)[ontology_id]
        tfs_by_term = {goid + "_" + g for goid, grp in
                       ontologies[0][ontologies[0]['ontology.group'] == ontology_id].groupby('id')
                       for g in set(grp['gene.name'])}

        print ontology_id, len(tfs_by_term)
        tfs_by_term_and_ontology[ontology_id] = tfs_by_term

    print 'iterating through queries dataframe and submitting cases...'
    print '# queries', queries_df.shape[0]
    queries_df = queries_df.reset_index(drop=True)

    print '# only with peaks approach', queries_df[queries_df['model'] == 'peaks'].shape[0]

    print 'query', query

    if query is not None and query.split("_")[-1] in {'peaks', 'tss', 'peaks+kmers'}:
        query_group = query.split("_")[-1]
        queries_df = queries_df[queries_df['model'].isin(set([query_group]))]
        print '# rows in dataframe (before for loop)', queries_df.shape[0]

    for ri, q in queries_df.iterrows():
        tf1, tf2, motif, ontology_id, group, thr = q['tf.remap.1'], q['tf.remap.2'], q['motif'], q['ontology'], q['model'], \
                                                   q['motif.mismatches.thr']

        if str(tf1) == 'nan' and str(tf2) == 'nan' and group != 'tss':
            continue

        if str(tf1) == 'nan' and str(tf2) == 'nan' and group == 'tss':
            tf1 = q['tf.cap.1']
            tf2 = q['tf.cap.2']

        if query == 'peaks' and group != 'peaks':
            continue

        # if group != 'peaks':
        #     continue


        code = "_".join([tf1, tf2, motif, ontology_id, group] if group != 'peaks' else [tf1, tf2, ontology_id, group])



        # print code
        if query is None:
            if ri < start or ri >= end:
                if ri < start:
                    continue
                if ri >= end:
                    break
        else:
            found = False
            if (not query in tf1 + "_" + tf2 + "_" + ontology_id + "_" + str(motif) + "_" + group) and\
                    (not query in tf2 + "_" +  tf1 + "_" + ontology_id + "_" + str(motif)) and\
                    (not query in tf1 + "_" + tf2 + "_" + ontology_id + "_" + group) and \
                    (not query in tf2 + "_" + tf1 + "_" + ontology_id + "_" + group) and\
                    (not query in tf1 + "_" + group) and\
                    (not query in tf2 + "_" + group):
                continue
            else:
                found = True
                if not group in query:
                    continue
            if not query in code:
                if not found:
                    continue

        print ''
        print ri, tf1, tf2, motif, ontology_id, group

        next_output_dir = join(output_dir, ontology_id, tf1 + "_" + tf2)
        if not exists(next_output_dir):
            makedirs(next_output_dir)
        output_path = join(next_output_dir, code + ".tsv.gz")

        print ri, exists(output_path), output_path
        if not exists(output_path):
            n_missing += 1

        motifs_remap_output_dir = join(output_dir, 'motifs_remap', tf1 + "_" + tf2)
        if not exists(motifs_remap_output_dir):
            makedirs(motifs_remap_output_dir)
        motifs_tss_output_dir = join(output_dir, 'motifs_tss', tf1 + "_" + tf2)
        if not exists(motifs_tss_output_dir):
            makedirs(motifs_tss_output_dir)
        stopat = None # 10 # None # 10

        zscore_nperm = 150
        nperm_by_core = 100
        ncores_perm = 1

        if exists(output_path) and group != 'peaks':
            # check that the nan exists
            df = DataFrameAnalyzer.read_tsv_gz(output_path)
            print df.shape[0]

        print 'preparing query IDs...'

        query_ids, priority_ids = GOCombinatorialBinding.get_query_and_priority_ids(tf1, tf2, ontology_id,
                                                                                    ontologies, tfs_by_term_and_ontology)



        check_id = 'DOID:0050745'
        print check_id, 'found', check_id in query_ids
        print check_id, 'found', check_id in priority_ids


        if user_query_ids is not None:
            query_ids = user_query_ids
            priority_ids = user_query_ids

        great = GREATAnalyzer(ontologies=ontologies)

        # the priority ids have to be checked here, by checking the thr for motif hits
        # assuming that the file exists: we reload it and we just query the next set of 500 elements that we need to solve
        print '\ncurrent IDs (before)', len(set(priority_ids))
        previous_ids = priority_ids

        previous_df, query_ids, priority_ids = GOCombinatorialBinding.update_query_and_priority_ids_from_previous_run(query_ids, priority_ids,
                                                                                                                      output_path, thr,
                                                                                                                      group=group)
        print 'current IDs (now)', len(priority_ids)
        print ''

        # if there are Ns in the query motif then we skip those when scoring (option skip_motif_N)
        if user_query_ids is not None:
            print user_query_ids
            query_ids = user_query_ids
            priority_ids = user_query_ids
        else:
            if len(priority_ids) == 0 and previous_df is not None and len(set(previous_df['id'])) > len(previous_ids) + 10:
                print 'all positives and enough negative cases have been generated for this query. Skip'
                continue


        peaks = None
        if group == 'peaks+kmers':
            peaks = GOCombinatorialBinding.get_peaks_intersection_remap(tf1, tf2, motif, motifs_remap_output_dir, skip_motif_N=True)
            print ''
            print '# of intersected REMAP peaks obtained...'
            print peaks.shape

        peaks_tss = None
        if group == 'tss':
            # repeat analysis with TSS
            print 'scanning motifs vs TSS'
            peaks_tss = GOCombinatorialBinding.get_tss_vs_motif(motif, motifs_tss_output_dir, skip_motif_N=True)
            print '# of TSS regions...'
            print peaks_tss.shape
            print ''

        peaks_cobound = None
        if group == 'peaks':
            # get a set with all the peaks belonging to either TF1 or TF2, indicating if those are intersected
            peaks_cobound = GOCombinatorialBinding.get_intersection_remap_peaks(tf1, tf2)
            print 'index:', ri
            print peaks_cobound.head()
            if peaks_cobound.shape[0] == 0:
                print 'empty dataframe. Skip case...'
                continue
            peaks_cobound['k.summit'] = peaks_cobound['strand']
            peaks_cobound['chr'] = peaks_cobound['chrom']
            del peaks_cobound['chrom']

        res = []

        print 'motifs ready. Starting motifs hits analysis...'

        maxtimemin = timemin / (ngroup + 1)
        print 'max amount of minutes to dedicate', maxtimemin

        t_start = get_time_now()

        t0 = get_time_now()
        print 'motif', motif, 'threshold:', thr
        print 'stopat', stopat

        next_peaks = {'tss': peaks_tss, 'peaks+kmers': peaks, 'peaks': peaks_cobound}[group]

        zscore_column = 'motif.selected' if group != 'peaks' else 'intersected'

        kwargs['peak_gene_associations_bkp_code'] = basename(output_path).replace(".tsv.gz", '') if group == 'peaks' else None

        try:
            great_res = GOCombinatorialBinding.calculate_zscores_regions_vs_gos(next_peaks, group, thr, query_ids=query_ids, priority_ids=priority_ids,
                                                                                maxtimemin=maxtimemin, stopat=stopat, nperm_by_core=nperm_by_core,
                                                                                ncores_perm=ncores_perm, zscore_column=zscore_column, zscore_nperm=zscore_nperm,
                                                                                min_ngenes=5, max_ngenes=1000, **kwargs)
        except Exception as e:
            print '\nERROR\nsomething went wrong in this query. Please verify...'
            print ri
            print q
            print e
            print '\n'

            continue



        if great_res.shape[0] == 0:
            continue
        print '# of entries calculated in this run:', great_res.shape[0]

        great_res['motif'] = motif

        if previous_df is not None:
            great_res = pd.concat([previous_df, great_res])

        print 'total minutes/seconds', get_timespan_minutes(t0), get_timespan_seconds(t0)

        DataFrameAnalyzer.to_tsv_gz(great_res, output_path)
        print 'once a job is done, finish the script...'
        print 'total time m/s', get_timespan_minutes(t_start), get_timespan_seconds(t_start)



    print 'n missing', n_missing

if __name__ == '__main__':
    # BSubFacade.prepare_multifunction_nohup(__file__, 362, 362 / 5)
    # exit()
    BSubFacade.prepare_jobarray_scripts(__file__, run, n_jobs=2720,# n_jobs=3490,
                                        n_jobs_max=1000,
                                        queue='htc', start=1, ncores=1, ngroup=214, # 214 * 2000 == 428000 queries
                                        mem=10000, qos='normal',
                                        time='05:00:00')

