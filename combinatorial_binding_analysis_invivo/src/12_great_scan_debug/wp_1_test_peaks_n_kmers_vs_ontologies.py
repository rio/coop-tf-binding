'''
Created on 3/16/2019

DESCRIPTION

@author: ignacio
'''
from lib.utils import *
from lib.plot_utils import *
from lib.GREATAnalyzer import GREATAnalyzer

maxtimemin = 1


# in chip seq
output_dir = '../../data/GREAT_zscores_downsampling_v2019'
tf1 = 'ETV6'
tf2 = 'FOXO1'
motif = 'TGTTGNCGGAAR'
motifs_remap_output_dir = join(output_dir, 'motifs_remap', tf1 + "_" + tf2)
bkp_motif_hits = join(motifs_remap_output_dir, motif + ".tsv.gz")
peaks = DataFrameAnalyzer.read_tsv_gz(bkp_motif_hits)

query_ids = {'DOID:0110489'}

query_ids = {'DOID:0050745'}
priority_ids = {}
zscore_nperm = 150
nperm_by_core = 100
ncores_perm = 1
stopat = None
great = GREATAnalyzer()

peaks['motif.selected'] = peaks['motif.score'] == max(peaks['motif.score'] - 1)


great_res = great.run_great_local(peaks[['chr', 'start', 'end', 'k.summit', 'motif.selected']],
                                  # query_ids=query_ids, stopat=5,
                                  query_ids=query_ids, stopat=stopat, priority_ids=priority_ids,
                                  nperm_by_core=nperm_by_core, ncores_perm=ncores_perm,
                                  maxtimemin=maxtimemin,
                                  zscore_column='motif.selected', zscore_nperm=zscore_nperm,  # zscore_nperm,
                                  zscore_ztransform=True,
                                  min_ngenes=10, max_ngenes=1000, save_bg_distr_plot=True, gof_method='poisson')

