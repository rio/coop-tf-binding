# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_table
import numpy as np
from functions import *

# deactivating warnings
import warnings
warnings.filterwarnings('ignore')

nshow = 200 * 3

df = read_multiple_tsv_gz('OAP')

# append results coming from excel table
xls = pd.read_excel('OAP/supp_data_7_OAP_values_strong_associations.xlsx', sheet_name='strong.assoc',
                    skiprows=19)

xls.columns = ['go.id', 'ontology', 'peaks+kmers.genes', 'peaks+kmers.peaks', 'peaks.genes', 'peaks.peaks',
               'tf.pair', 'Ng.tss', 'name', 'tf1', 'tf2', 'tf1.in.ontology', 'tf2.in.ontology',
               'tf1.or.tf2', 'tf1.and.tf2', 'ontology.association.probability', 'label']

oap = 'ontology.association.probability'
# grp = grp[grp[ylabel] | grp['tf1.or.tf2']]
df[oap] = np.where(np.isnan(df['tf1.and.tf2.score.peaks+kmers']),
                     df['tf1.or.tf2.score.peaks+kmers'], df['tf1.and.tf2.score.peaks+kmers'])
df = df.rename(columns={'tf1.found': 'tf1.in.ontology', 'tf2.found': 'tf2.in.ontology',
                        'oap': oap})

xls = xls[[c for c in xls if c in df]]



cols = ['tf.pair', 'go.id', 'name', 'ontology', 'peaks+kmers.genes', 'peaks+kmers.peaks',
        'peaks.genes', 'peaks.peaks', oap, 'tf1.in.ontology', 'tf2.in.ontology', 'tf1.and.tf2', 'tf1.or.tf2']
df = df[cols]

df = pd.concat([df, xls]).reset_index(drop=True)
for k in ['tf1', 'tf2']:
    if k in df:
        del df[k]

df = df[cols]
df_by_ont = {ont: grp for ont, grp in df.groupby('ontology')}


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server

colors = {
    'background': '#F9F9F9',
    'text': '#4f6770'
}

styles = {
    'pre': {
        'border': 'thin lightgrey solid',
        'overflowX': 'scroll'
    }
}

available_indicators = {"peaks+kmers.genes", "peaks+kmers.peaks", "peaks.peaks", "peaks.genes"}

nmin = 100


markdown_text = '''## Usage
- Use the controls to visualize count based Z-scores linking human TF-pairs and ontology data.
- Filter/sort columns to highlight specific TFs and TF-pairs of interest (see [column description](https://www.dropbox.com/s/v9u9wm98tag2yhx/README_scores.txt?dl=0) for defititions).
    * e.g.1. Search **NANOG** in column **`tf.pair`** (case sensitive), or **>=5.0** in column **`peaks+kmers.genes`**
    * e.g.2. Search **>0.2** in **`ontology.association.probability`**, and **true** in `tf1.and.tf2` to get strong ontology associations for category ***TF1 and TF2*** (see manuscript for details).
    * e.g.3. Search **>0.4** in **`ontology.association.probability`**, **false** in **`tf1.and.tf2`**, and **true** in **`tf1.or.tf2`** to get strong ***TF1 or TF2*** ontology associations.
- Please refer to the ontology database IDs for more information on target genes.
- You can calculate custom Z-scores for custom peaks/genes using the [Git source](https://git.embl.de/rio/coop-tf-binding).
- Raw predictions tables can be obtained [here.](http://www.zaugg.embl.de/data-and-tools/coop-tf-binding/)
- Not all human TFs are included in this catalog. Please refer to the manuscript's Methods for selection criteria.
    * If you're interested in a TF or TF-pair not shown please let us know and we'll readily include it to this interface.
- Feedback/errors/requests: Please report via [e-mail](mailto:ignacio.ibarra@embl.de), or Twitter [@ilibarra](https://twitter.com/ilibarra).

'''

app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    html.H2(
        children='TF-pairs and ontology associations',
        style={
            'textAlign': 'center',
            'color': colors['text']
        }
    ),

    html.Div(children=[dcc.Markdown('scores from Ibarra *et al.* (2020)')], style={
        'textAlign': 'center',
        'color': colors['text']
    }),

    html.Div([dcc.Markdown(children=markdown_text)],
             style={'width': 400, 'float': 'left', 'display': 'inline-block'}),

    html.Div([

        html.Div([
            dcc.Markdown('## Dashboard'),
            dcc.Markdown('**X-axis**'),
            dcc.Dropdown(
                id='xaxis-column',
                options=[{'label': "Number of %s using %s models [%s]" % (i.split(".")[-1],
                                                                          i.split(".")[0],
                                                                          "Z-score"), 'value': i}
                         for i in available_indicators],
                value="peaks+kmers.genes",
            ),
            dcc.Markdown('**Y-axis**'),
            dcc.Dropdown(
                id='yaxis-column',
                options=[{'label': "Number of %s using %s models [%s]" % (i.split(".")[-1],
                                                                          i.split(".")[0],
                                                                          "Z-score"), 'value': i}
                         for i in available_indicators],
                value="peaks+kmers.peaks",
            ),
            dcc.Markdown('**Ontology Database**'),
            dcc.Checklist(
                id='checkbox',
                options=[
                    {'label': 'DISEASES',
                     'value': 'DISEASES'},
                    {'label': 'GO', 'value': 'GO'},
                    {'label': 'HPO', 'value': 'HPO'}
                ],
                value=['DISEASES', 'GO', 'HPO']
            ),
            dcc.Markdown('Please activate/deactivate to see in scatter.'),
            # html.Label('Scatter parameters (please wait a bit between changes)'),
            dcc.Markdown('**Max number of associations to show (per ontology)**'),
            dcc.Slider(id='nmax-slider',
                       min=nmin,
                       max=1000,
                       marks={i * nmin: str(i * 100) for i in range(1, 11)},
                       value=200,
                       step=None,
                       ),


            # html.Label('Sources'),
            # html.Label(['Pletscher-Frankild et al 2015', html.A('link', href='https://diseases.jensenlab.org')]),
            # html.Label(['The Gene Ontology Consortium', html.A('link', href='http://geneontology.org/')]),
            # html.Label(['Köhler et al 2018', html.A('link', href='https://hpo.jax.org/')])
        ],
            style={'width': 450, 'float': 'left', 'display': 'inline-block',
                   'margin': {'l': 10}}),
    ]),

    # dcc.Graph(id='scatter', style={"width": "75vh", "height": '50vh'}),
    # html.Div([dcc.Graph(id='scatter'),],
    #          style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
    html.Div(id='datatable-interactivity-container',
             style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),


    html.Div(id='datatable-container', children=[dash_table.DataTable(
            id='datatable-interactivity',
            columns=[
                {"name": i, "id": i, "deletable": False, "selectable": False}
                for i in df.columns
            ],
            data=df[df['tf1.and.tf2'] | df['tf1.or.tf2']].sample(nshow).to_dict('records'),
            # editable=True,
            filter_action="native",
            sort_action="native",
            sort_mode="multi",
            selected_columns=[],
            selected_rows=[],
            page_action="native",
            page_current= 0,
            page_size= 10)],
             style={'display': 'inline-block'})

    # html.Div([dash_table.DataTable(id='table')])
    # generate_table(df)
])


@app.callback(
    Output('datatable-interactivity-container', 'children'),
    #  Output('datatable-container', 'children')],
    [Input('xaxis-column', 'value'),
     Input('yaxis-column', 'value'),
     Input('checkbox', 'value'),
     Input('datatable-interactivity', "derived_virtual_data"),
     Input('datatable-interactivity', "derived_virtual_selected_rows"),
     Input('nmax-slider', 'value')])
def update_graph(xlab, ylab,
                 ontologies, rows, derived_virtual_selected_rows, nmax_show):
    print 'update graph'
    # print ontologies

    # When the table is first rendered, `derived_virtual_data` and
    # `derived_virtual_selected_rows` will be `None`. This is due to an
    # idiosyncracy in Dash (unsupplied properties are always None and Dash
    # calls the dependent callbacks when the component is first rendered).
    # So, if `rows` is `None`, then the component was just rendered
    # and its value will be the same as the component's dataframe.
    # Instead of setting `None` in here, you could also set
    # `derived_virtual_data=df.to_rows('dict')` when you initialize
    # the component.

    sel_keys = set()

    if rows is None:
        print 'rows is none. Generate frame with defaults'
        dff = df[df['tf1.and.tf2'] | df['tf1.or.tf2']].sample(nmax_show)
    else:
        print 'updating according to rows'
        dff = pd.DataFrame(rows, columns=df.columns)
        print dff.shape[0]

    if dff.shape[0] > 0:
        sel_keys = set(dff['go.id'] + "_" + dff['tf.pair'])
    elif dff.shape[0] == 0:
        sel_keys = None

    # print '\nplotting'
    # print 'amount of selected keys....', len(sel_keys)


    traces = []
    for ont in df_by_ont:
        sel = df_by_ont[ont]
        if ont not in set(ontologies):
            continue
        print 'generating traces for', ont, sel.shape[0]

        if sel_keys is not None and len(sel_keys) != 0:
            sel = sel[(sel['go.id'] + "_" + sel['tf.pair']).isin(sel_keys)]
        else:
            sel = sel.head(0)

        # limit to a maximum quota
        sel = sel.sample(min(sel.shape[0], int(nmax_show)))
        sizes = np.interp(sel[oap], (sel[oap].min(), sel[oap].max()), (2, 15))
        color = sel['ontology'].map({'DISEASES':'#e41a1c', 'GO': '#377eb8', 'HPO': '#4daf4a'})
        traces.append(dict(
            x=sel[sel['tf1.and.tf2'] | sel['tf1.or.tf2']][xlab],
            y=sel[sel['tf1.and.tf2'] | sel['tf1.or.tf2']][ylab],
            text=sel['tf.pair'] + "<br>" + sel['name'] + "<br>" +
                 sel['go.id'] + "<br>" + 'OAP=' + sel[oap].astype(str),
            mode='markers',
            opacity=0.6,
            marker=dict(size=sizes, color=color),
            name=ont + ' (N=%i)' % sel[sel['tf1.and.tf2'] | sel['tf1.or.tf2']].shape[0],
        ))

    return [dcc.Graph(id='scatter', figure={
        'data': traces,
        'layout': dict(
            xaxis={'title': "<b>Number of %s using %s models [%s]" % (xlab.split(".")[-1],
                                                                          xlab.split(".")[0],
                                                                          "Z-score</b>")},
            yaxis={'title': "<b>Number of %s using %s models [%s]" % (ylab.split(".")[-1],
                                                                          ylab.split(".")[0],
                                                                          "Z-score</b>")},
            margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
            legend={'x': 0, 'y': 1},
            hovermode='closest'
        )})]


operators = [['ge ', '>='],
             ['le ', '<='],
             ['lt ', '<'],
             ['gt ', '>'],
             ['ne ', '!='],
             ['eq ', '='],
             ['contains '],
             ['datestartswith ']]
def split_filter_part(filter_part):
    for operator_type in operators:
        for operator in operator_type:
            if operator in filter_part:
                name_part, value_part = filter_part.split(operator, 1)
                name = name_part[name_part.find('{') + 1: name_part.rfind('}')]

                value_part = value_part.strip()
                v0 = value_part[0]
                if (v0 == value_part[-1] and v0 in ("'", '"', '`')):
                    value = value_part[1: -1].replace('\\' + v0, v0)
                else:
                    try:
                        value = float(value_part)
                    except ValueError:
                        value = value_part

                # word operators need spaces after them in the filter string,
                # but we don't want these later
                return name, operator_type[0].strip(), value

    return [None] * 3

@app.callback(Output('datatable-interactivity','data'),
              [Input('datatable-interactivity', "filter_query"),])
def update_table(filter):
    print 'update table...'
    dff = df

    if filter is not None:
        filtering_expressions = filter.split(' && ')
        print filtering_expressions
        for filter_part in filtering_expressions:
            col_name, operator, filter_value = split_filter_part(filter_part)
            if operator in ('eq', 'ne', 'lt', 'le', 'gt', 'ge'):
                # these operators match pandas series operator method names
                if str(filter_value).replace('.','',1).isdigit():
                    dff = dff.loc[getattr(dff[col_name], operator)(filter_value)]
            elif operator == 'contains':
                dff = dff.loc[dff[col_name].str.contains(filter_value)]
            elif operator == 'datestartswith':
                # this is a simplification of the front-end filtering logic,
                # only works with complete fields in standard format
                dff = dff.loc[dff[col_name].str.startswith(filter_value)]
        return dff.sample(min(dff.shape[0], nshow)).to_dict('records')
    else:
        return dff[df['tf1.and.tf2'] | df['tf1.or.tf2']].sample(nshow).to_dict('records')

if __name__ == '__main__':
    # app.run_server(debug=True, port=8051)
    app.run_server()
